# StoreLocator


## Installation Instructions
1. *Create a database backup* - There may be some issues with creating the EAV structure.
2. Install using composer.
3. Run the setup scripts using `n98-magerun.phar sys:setup:run` - There is an issue with creating tables running through the browser.
4. Clear cache.

## Branches / releases
* Code in releases is stable, reliably working in a production environment.
* Code in `master` branch should be stable but its not guaranteed that it is deployed to a production environment.
* In your composer `require` use something like `"aligent/storelocator":"~1.0"`

## Config
See System -> Configuration -> Aligent -> Stores.

## Maintainer
* Maintainer zain@aligent.com.au