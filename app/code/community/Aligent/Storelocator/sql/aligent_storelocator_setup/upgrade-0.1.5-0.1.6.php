<?php

$this->startSetup()->run("-- DROP TABLE IF EXISTS {$this->getTable('aligent_storelocator/searchlog')};
CREATE TABLE {$this->getTable('aligent_storelocator/searchlog')} (
  `log_id` int(11) NOT NULL auto_increment,
  `ip` varchar(64) NOT NULL default '0',
  `time` int(11) NOT NULL default '0',
  PRIMARY KEY  (`log_id`),
  KEY `ip` (`ip`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Store Locator search function log storage table';
")->endSetup();