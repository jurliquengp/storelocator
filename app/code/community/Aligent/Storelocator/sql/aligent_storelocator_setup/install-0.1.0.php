<?php
/**
 * install-0.1.0.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/** @var Aligent_Storelocator_Model_Entity_Setup $installer */
$installer = $this;

/**
 * Create table 'aligent_storelocator/eav_attribute'
 */
$table = $installer->getConnection()
                   ->newTable($installer->getTable('aligent_storelocator/eav_attribute'))
                   ->addColumn(
                   'attribute_id',
                   Varien_Db_Ddl_Table::TYPE_SMALLINT,
                   null,
                   array(
                       'identity' => false,
                       'unsigned' => true,
                       'nullable' => false,
                       'primary'  => true,
                   ),
                   'Attribute Id'
    )
                   ->addColumn(
                   'is_visible',
                   Varien_Db_Ddl_Table::TYPE_SMALLINT,
                   null,
                   array(
                       'unsigned' => true,
                       'nullable' => false,
                       'default'  => '1',
                   ),
                   'Is Visible'
    )
                   ->addColumn(
                   'input_filter',
                   Varien_Db_Ddl_Table::TYPE_TEXT,
                   255,
                   array(),
                   'Input Filter'
    )
                   ->addColumn(
                   'validate_rules',
                   Varien_Db_Ddl_Table::TYPE_TEXT,
                   '64k',
                   array(),
                   'Validate Rules'
    )
                   ->addColumn(
                   'sort_order',
                   Varien_Db_Ddl_Table::TYPE_INTEGER,
                   null,
                   array(
                       'unsigned' => true,
                       'nullable' => false,
                       'default'  => '0',
                   ),
                   'Sort Order'
    )
                   ->addForeignKey(
                   $installer->getFkName(
                             'aligent_storelocator/eav_attribute',
                             'attribute_id',
                             'eav/attribute',
                             'attribute_id'
                   ),
                   'attribute_id',
                   $installer->getTable('eav/attribute'),
                   'attribute_id',
                   Varien_Db_Ddl_Table::ACTION_CASCADE,
                   Varien_Db_Ddl_Table::ACTION_CASCADE
    )
                   ->setComment('Storelocator Eav Attribute');
$installer->getConnection()->createTable($table);

/**
 * Create table 'aligent_storelocator/eav_attribute_website'
 */
$table = $installer->getConnection()
                   ->newTable($installer->getTable('aligent_storelocator/eav_attribute_website'))
                   ->addColumn(
                   'attribute_id',
                   Varien_Db_Ddl_Table::TYPE_SMALLINT,
                   null,
                   array(
                       'unsigned' => true,
                       'nullable' => false,
                       'primary'  => true,
                   ),
                   'Attribute Id'
    )
                   ->addColumn(
                   'website_id',
                   Varien_Db_Ddl_Table::TYPE_SMALLINT,
                   null,
                   array(
                       'unsigned' => true,
                       'nullable' => false,
                       'primary'  => true,
                   ),
                   'Website Id'
    )
                   ->addColumn(
                   'is_visible',
                   Varien_Db_Ddl_Table::TYPE_SMALLINT,
                   null,
                   array(
                       'unsigned' => true,
                   ),
                   'Is Visible'
    )
                   ->addColumn(
                   'is_required',
                   Varien_Db_Ddl_Table::TYPE_SMALLINT,
                   null,
                   array(
                       'unsigned' => true,
                   ),
                   'Is Required'
    )
                   ->addIndex(
                   $installer->getIdxName('aligent_storelocator/eav_attribute_website', array('website_id')),
                   array('website_id')
    )
                   ->addForeignKey(
                   $installer->getFkName(
                             'aligent_storelocator/eav_attribute_website',
                             'attribute_id',
                             'eav/attribute',
                             'attribute_id'
                   ),
                   'attribute_id',
                   $installer->getTable('eav/attribute'),
                   'attribute_id',
                   Varien_Db_Ddl_Table::ACTION_CASCADE,
                   Varien_Db_Ddl_Table::ACTION_CASCADE
    )
                   ->addForeignKey(
                   $installer->getFkName(
                             'aligent_storelocator/eav_attribute_website',
                             'website_id',
                             'core/website',
                             'website_id'
                   ),
                   'website_id',
                   $installer->getTable('core/website'),
                   'website_id',
                   Varien_Db_Ddl_Table::ACTION_CASCADE,
                   Varien_Db_Ddl_Table::ACTION_CASCADE
    )
                   ->setComment('Storelocator Eav Attribute Website');
$installer->getConnection()->createTable($table);

/**
 * Create table 'aligent_storelocator/form_attribute'
 */
$table = $installer->getConnection()
                   ->newTable($installer->getTable('aligent_storelocator/form_attribute'))
                   ->addColumn(
                   'form_code',
                   Varien_Db_Ddl_Table::TYPE_TEXT,
                   32,
                   array(
                       'nullable' => false,
                       'primary'  => true,
                   ),
                   'Form Code'
    )
                   ->addColumn(
                   'attribute_id',
                   Varien_Db_Ddl_Table::TYPE_SMALLINT,
                   null,
                   array(
                       'unsigned' => true,
                       'nullable' => false,
                       'primary'  => true,
                   ),
                   'Attribute Id'
    )
                   ->addIndex(
                   $installer->getIdxName('aligent_storelocator/form_attribute', array('attribute_id')),
                   array('attribute_id')
    )
                   ->addForeignKey(
                   $installer->getFkName(
                             'aligent_storelocator/form_attribute',
                             'attribute_id',
                             'eav/attribute',
                             'attribute_id'
                   ),
                   'attribute_id',
                   $installer->getTable('eav/attribute'),
                   'attribute_id',
                   Varien_Db_Ddl_Table::ACTION_CASCADE,
                   Varien_Db_Ddl_Table::ACTION_CASCADE
    )
                   ->setComment('Storelocator Form Attribute');
$installer->getConnection()->createTable($table);

$installer->addEntityType(
          'storelocator_storelocation',
          array(
              //entity_mode is the URL you'd pass into a Mage::getModel() call
              'entity_model'               => 'aligent_storelocator/storelocation',
              //blank for now
              'attribute_model'            => 'aligent_storelocator/attribute',
              //table refers to the resource URI complexworld/eavblogpost
              //<complexworld_resource_eav_mysql4>...<eavblogpost><table>eavblog_posts</table>
              'table'                      => 'aligent_storelocator/storelocations',
              //blank for now, but can also be eav/entity_increment_numeric
              'increment_model'            => '',
              //appears that this needs to be/can be above "1" if we're using eav/entity_increment_nu
              'increment_per_store'        => '0',
              'additional_attribute_table' => 'aligent_storelocator/eav_attribute'
          )
);

$installer->createEntityTables(
          $this->getTable('aligent_storelocator/storelocations'),
          array(
              'types' => array(
                  'latlong' => array(Varien_Db_Ddl_Table::TYPE_DECIMAL, '9,6')
              )
          )
);

$installer->installEntities();

foreach (array(
             'name',
             'address1',
             'address2',
             'city',
             'state',
             'postcode',
             'latitude',
             'longitude'
         ) as $vAttributeCode) {
    $iAttributeId = $installer->getAttributeId('storelocator_storelocation', $vAttributeCode);
    try {
        $installer->run(
                  <<<SQL
                  INSERT INTO {$installer->getTable('aligent_storelocator/form_attribute')}(form_code, attribute_id)
VALUES('adminhtml_storelocation', $iAttributeId)
SQL
        );
    } catch (Exception $e) {
        // Do nothing
        throw $e;
    }
}

