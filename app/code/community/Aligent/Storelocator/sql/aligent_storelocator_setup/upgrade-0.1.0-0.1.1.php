<?php

/** @var Aligent_Storelocator_Model_Entity_Setup $setup */
$setup = $this;
$setup->startSetup();
$vType = 'storelocator_storelocation';

$vAttrName = 'store_ids';

$setup->removeAttribute($vType, $vAttrName);

$setup->addAttribute($vType, $vAttrName, array(
    'label'                     => 'Stores',
    'input'                     => 'multiselect',
    'source'                    => 'aligent_storelocator/attribute_source_stores',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'                   => true,
    'required'                  => false,
    'unique'                    => false,
    'user_defined'              => true,
    'is_user_defined'           => true
));

$vAttrName = 'description';

$setup->removeAttribute($vType, $vAttrName);

$setup->addAttribute($vType, $vAttrName, array(
    'label'                     => 'Description',
    'input'                     => 'textarea',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'                   => true,
    'required'                  => false,
    'unique'                    => false,
    'user_defined'              => true,
    'is_user_defined'           => true
));

$vAttrName = 'store_type';

$setup->removeAttribute($vType, $vAttrName);

$setup->addAttribute($vType, $vAttrName, array(
    'label'                     => 'Type',
    'input'                     => 'multiselect',
    'source'                    => 'aligent_storelocator/attribute_source_storetypes',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'                   => true,
    'required'                  => false,
    'unique'                    => false,
    'user_defined'              => true,
    'is_user_defined'           => true
));


foreach (array(
             'store_ids',
             'description',
             'store_type'
         ) as $vAttributeCode) {
    $iAttributeId = $setup->getAttributeId('storelocator_storelocation', $vAttributeCode);
    try {
        $setup->run(
            <<<SQL
                  INSERT INTO {$setup->getTable('aligent_storelocator/form_attribute')}(form_code, attribute_id)
VALUES('adminhtml_storelocation', $iAttributeId)
SQL
        );
    } catch (Exception $e) {
        // Do nothing
    }
}


$setup->endSetup();

