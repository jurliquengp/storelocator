<?php
/**
 * upgrade-0.1.2-0.1.3.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Zain <zain@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/** @var Aligent_Storelocator_Model_Entity_Setup $setup */
$setup = $this;
$setup->startSetup();

$vType = 'storelocator_storelocation';
$vAttrName = 'country';

//will allow to edit it and will also add field in eav
$setup->addAttribute($vType, $vAttrName, array(
    'label'                     => 'Country Code',
    'input'                     => 'text',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'                   => true,
    'required'                  => false,
    'unique'                    => false,
    'user_defined'              => true,
    'is_user_defined'           => true
));


$locationFields = array('country',);
//add fields in admin form
foreach ($locationFields as $vAttributeCode) {
    $iAttributeId = $setup->getAttributeId('storelocator_storelocation', $vAttributeCode);
    try {
        $setup->run(
            <<<SQL
                  INSERT INTO {$setup->getTable('aligent_storelocator/form_attribute')}(form_code, attribute_id)
VALUES('adminhtml_storelocation', $iAttributeId)
SQL
        );
    } catch (Exception $e) {
        // Do nothing
    }
}

$setup->endSetup();