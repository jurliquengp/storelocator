<?php
/**
 * upgrade-0.1.1-0.1.2.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/** @var Aligent_Storelocator_Model_Entity_Setup $installer */
$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

$eavAttributeTableName = $installer->getTable('aligent_storelocator/eav_attribute');

$connection->addColumn(
           $eavAttributeTableName,
           'is_wysiwyg_enabled',
           array(
               'comment'  => 'Is WYSWIG enabled',
               'type'     => Varien_Db_Ddl_Table::TYPE_SMALLINT,
               'nullable' => false,
               'default'  => 0,
           )
);

$connection->addColumn(
           $eavAttributeTableName,
           'is_html_allowed_on_front',
           array(
               'comment'  => 'Is HTML allowed on front',
               'type'     => Varien_Db_Ddl_Table::TYPE_SMALLINT,
               'nullable' => false,
               'default'  => 0,
           )
);

$installer->endSetup();