<?php
/**
 * Geo.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Helper_Import
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Zain <zain@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Helper_CsvImport extends Mage_Core_Helper_Abstract
{
    public $csvPath;
    public $dataMap = array();
    public $sheetCode = array();
    public $currentSheetCode = '';
    public $skipCheckFunction = array();
    public $aCodeStoreIdMap = array();

    public function __construct()
    {
        $this->addConfigData();
        $this->addJsonCacheForMatches();
    }

    /**
     *  overwrite this method to provide custom field maps
     * can add custom skip by appending $this->skipCheckFunction[] row is passed by reference to it
     * can manipulate row and skip some rows in csv for example if sku not found
     * can provide $auData['matchFieldFilter'] to update existing records using a criteria
     * use $auData['deleteOld'] to delete existing records if needed
     */
    public function addConfigData()
    {
        $auData = array();
        //required if running as a shell script
        $auData['csvPath'] = BP . '/shell/StoreLocatorImport/resource/source.csv';;


        //not required as deleting old records and creating new
//        $auData['matchFieldFilter'] = array();
//        $auData['matchFieldFilter']['php'] = array(
//            'name' => array('eq' => '{name}'),
//            'city' => array('eq' => '{city}'),
//        );

        //map between sheet headings and internal / dataModel field names
        $auData['columnMap'] = array(
            'Store Name' => 'name',
            'Address' => 'address1',
            'City' => 'city',
            'State' => 'state',
            'Zip' => 'postcode',
            'Phone Number' => 'phone',
            'store' => 'store_codes',
            'Store Type' => 'store_type',
            'Hours Of Operation' => 'open_hours',
        );

        $auData['requiredColumns'] = array(
            'name',
            'address1',
            'city',
        );

        $auData['storeTypeOptions'] = array();
        $auData['storeTypeOptions']['type_field'] = 'store_type';
        $auData['storeTypeOptions']['typeColumns']    = array(
            'Spa',
            'Department Store',
            'Duty Free',
            'Authorised Retailer',
        );

        //find replace values in fields as per format needed in data model
        $auData['fieldFixes'] = array(
            'store_type' => array(
                'Retailer' => 'Retail',
            ),
        );

        //model we need to save
        $auData['modelClass'] = 'aligent_storelocator/storelocation';
        $auData['deleteOld'] = true;
        $this->dataMap['combined'] = $auData;

        $this->skipCheckFunction[] ='addStoreTypes';

    }



    public function addJsonCacheForMatches()
    {
        foreach ($this->dataMap as $sheetCode => $sheetData) {
            if (empty($sheetData['matchFieldFilter']['php']))
                continue;
            $matchFilter = $sheetData['matchFieldFilter']['php'];
            $this->dataMap[$sheetCode]['matchFieldFilter']['json'] = json_encode($matchFilter);
        }
    }

    function csvToFlatArray($csvPath, $delimiter = ',', $columnMap, $skipFunction, $requiredColumns)
    {
        if (!file_exists($csvPath)) {
            throw new Exception('file not found ' . $csvPath);
        }

        $handle = fopen($csvPath, 'r');

        $rows = array();
        $header = $data = fgetcsv($handle, null, $delimiter);
        if (!$header) {
            throw new Exception('no header in file' . $csvPath);
        }

        if ($this->missingRequiredColumns($requiredColumns, $header, $columnMap)) {
            throw new Exception('Csv format is not correct');
        }
        while (($data = fgetcsv($handle, null, $delimiter)) !== FALSE) {
            $row = array();
            foreach ($header as $key => $heading) {
                $heading = trim($heading);
                if (empty($columnMap[$heading])) {
                    continue;
                }
                $row[$columnMap[$heading]] = (isset($data[$key])) ? trim($data[$key]) : '';
            }
            $skipIt = false;
            if ($skipFunction) {
                $skipIt = call_user_func_array(array($this, $skipFunction), array(&$row));
                if ($skipIt)
                    continue;
            }
            if (!$skipIt){
                $rows[] = $row;
            }
        }
        fclose($handle);

        return $rows;
    }

    public function missingRequiredColumns($requiredColumns, $header, $columnMap)
    {
        $missingColumns = $requiredColumns;
        foreach ($header as $sheetLabel) {
            $sheetLabel = trim($sheetLabel);
            //valid mapping for sheet not provided
            if (empty($columnMap[$sheetLabel]))
                continue;
            $fieldName = $columnMap[$sheetLabel];
            $foundKey = array_search($fieldName, $requiredColumns);
            //remove from array if required column is in the sheet
            if ($foundKey !== false)
                unset($missingColumns[$foundKey]);
        }
        return $missingColumns;
    }

    public function fixFields(&$row)
    {
        if (empty($this->dataMap[$this->currentSheetCode]['fieldFixes']))
            return;
        $fieldFixes = $this->dataMap[$this->currentSheetCode]['fieldFixes'];
        //fix inconsistancies
        foreach ($fieldFixes as $field => $findReplace) {
            if (empty($row[$field]))
                continue;
            $sheetValue = $row[$field];
            if (is_string($sheetValue) && isset($fieldFixes[$field][$sheetValue])) {
                $row[$field] = $fieldFixes[$field][$sheetValue];
            }
        }
    }

    public function skipRow(&$row)
    {
        $this->fixFields($row);
        return $this->customSkipFunctions($row);
    }

    public function customSkipFunctions(&$row)
    {
        $skipRow = false;
        foreach ($this->skipCheckFunction as $functionName) {
            $skipReturn = call_user_func_array(array($this, $functionName), array(&$row));
            if ($skipReturn)
                return true;
        }
        return $skipRow;
    }

    public function deleteOldData()
    {
        if (empty($this->dataMap[$this->currentSheetCode]['deleteOld']))
            return;

        $modelClass = $this->dataMap[$this->currentSheetCode]['modelClass'];
        $this->deleteModelRecords($modelClass);
    }

    public function deleteModelRecords($modelClass)
    {
        /** @var $modelCollection Aligent_Storelocator_Model_Resource_Storelocation_Collection */
        $modelCollection = Mage::getModel($modelClass)->getCollection();
        if (method_exists($modelCollection, 'delete')) {
            $modelCollection->delete();
        }
        else {
            foreach ($modelCollection as $singleRow) {
                /** @var $singleRow Aligent_Storelocator_Model_Storelocation */
                try {
                    $singleRow->delete();
                } catch (Exception $e) {
                    echo "Unable to delete #" . $singleRow->getId() . " because: " . $e->getMessage() . "<br/>\n";
                }
            }
        }
    }

    /**
     *
     * If criteria provided to update existing rows, will return first instance which matches criteria
     * Otherwise will return an instance of new record
     *
     * @param $row
     * @return Aligent_Storelocator_Model_Storelocation
     */
    public function getMatchingRecord($row)
    {

        $modelClass = $this->dataMap[$this->currentSheetCode]['modelClass'];

        if (empty($this->dataMap[$this->currentSheetCode]['matchFieldFilter']['json']))
            return Mage::getModel($modelClass);;

        $filter = $this->dataMap[$this->currentSheetCode]['matchFieldFilter']['json'];

        //find replace field patter {field_name} with actual values, to be used in addFieldToFilter
        $searchArray = array();
        $replaceArray = array();
        foreach ($row as $field => $value) {
            $searchArray[] = '{' . $field . '}';
            $escapedValue = $this->escapeJsonString($value);
            $replaceArray[] = $escapedValue;
        }
        $filter = str_replace($searchArray, $replaceArray, $filter);
        $filter = json_decode($filter, true);
        if (!$filter)
            return Mage::getModel($modelClass);;


        /** @var $importModel Aligent_Storelocator_Model_Storelocation */
        $importModel = Mage::getModel($modelClass);
        $storeLocatorCollection = $importModel->getCollection();
        foreach ($filter as $field => $collectionFilter) {
            $storeLocatorCollection->addFieldToFilter($field, $collectionFilter);
        }

        if ($count = count($storeLocatorCollection)) {
            return $storeLocatorCollection->getFirstItem();
        }
        else {
            return Mage::getModel($modelClass);
        }
    }

    public function importAll()
    {
        foreach ($this->dataMap as $sheetCode => $sheetData) {
            $this->currentSheetCode = $sheetCode;
            $this->deleteOldData();
            $this->importSheet($sheetData);
        }

        //echo completed message, when in commandline or when testing it in only_dev script
        if ((php_sapi_name() == 'cli') || (isset($_SERVER['SCRIPT_NAME']) && (in_array($_SERVER['SCRIPT_NAME'], array('/only_dev.php'))))) {
            echo "<br/> import complete  File:" . __FILE__ . " line:" . __LINE__ . "<br/>\r\n";
        }
    }

    public function importSheet(&$sheetData)
    {
        $flatArray = $this->csvToFlatArray($sheetData['csvPath'], ',', $sheetData['columnMap'], 'skipRow', $sheetData['requiredColumns']);
        foreach ($flatArray as $row) {
            $matchingStore = $this->getMatchingRecord($row);
            $matchingStore->addData($row);
            if ($this->beforeSave($matchingStore,$row)) {
                $saveStatus = $matchingStore->save();
                $this->afterSave($matchingStore, $saveStatus,$row);
            }

        }
    }

    function escapeJsonString($str)
    {
        $from = array('"'); // Array of values to replace
        $to = array('\\"'); // Array of values to replace with

        // Replace the string passed
        return str_replace($from, $to, $str);
    }

    public function beforeSave($matchingRecord,&$row){
        return true;
    }

    public function afterSave($matchingRecord, $saveStatus,&$row){

    }

    public function addStoreTypes(&$row)
    {
        if (empty($this->dataMap[$this->currentSheetCode]['storeTypeOptions']['typeColumns']))
            return;
        $storeTypes = $this->dataMap[$this->currentSheetCode]['storeTypeOptions']['typeColumns'];
        $storeTypeField = $this->dataMap[$this->currentSheetCode]['storeTypeOptions']['type_field'];
        $typeList = array();
        // key is added to avoid duplication
        if (!empty($row[$storeTypeField])){
            $typeList[$row[$storeTypeField]] = $row[$storeTypeField];
        }

        //loop storeTypes in config, if column has value, add it for implode list
        foreach ($storeTypes as $typeName) {
            if (isset($row[$typeName])){
                if ($typeValue = trim($row[$typeName])){
                    $typeList[$typeName] = $typeName;
                }
                //we don't to save store types as separate fields in data model
                unset($row[$typeName]);
            }
        }
        $row[$storeTypeField] = implode(',',$typeList);
    }
    public function addLatitudeLongitude(&$row)
    {
        $address = $this->getAddress($row);
        $geoLocation = $this->getGeoLocation($address);
        if (!$geoLocation){
            $this->debugMessage("could not find geo location for $address");
            return;
        }

        $row['latitude'] = $geoLocation->lat;
        $row['longitude'] = $geoLocation->lng;
    }
    public function getAddress($row)
    {
        $addressFields = array(
            'address1',
            'address2',
            'city',
            'state',
            'postcode',
            'country',
        );
        $addressParts = array();
        foreach ($addressFields as $fieldName) {
            if (!empty($row[$fieldName])){
                $addressParts[$fieldName] = $row[$fieldName];
            }
        }
        $address = implode(' ', $addressParts);
        return $address;
    }
    public function debugMessage($vMessage)
    {
        Mage::log($vMessage, Zend_Log::WARN, 'store_locator.log');
        if (php_sapi_name() == 'cli') {
            echo "$vMessage  File:" . __FILE__ . " line:" . __LINE__ . "\n";
        }
    }

    public function getGeoLocation($address)
    {
        $cache = Mage::app()->getCache();
        $key = sha1("latitude longitude ver 1" . $address);
        $geoLocation = $cache->load($key);
        if ($geoLocation && (is_array($geoLocation = json_decode($geoLocation)) || $geoLocation)) {
            return $geoLocation;
        }
        $helper = Mage::helper('aligent_storelocator/geo');
        $geoLocation = $helper->getGeolocationFromAddress($address);
        if (!$geoLocation && $helper->errorResponse == 'OVER_QUERY_LIMIT'){
            sleep(1);
            $geoLocation = $helper->getGeolocationFromAddress($address);
        }
        if ($geoLocation) {
            $cache->save(json_encode($geoLocation), $key, array('geo_location_from_address'));
            return $geoLocation;
        }
        else{
            $this->debugMessage("API returned following error status: " . $helper->errorResponse);
            if ($helper->errorResponse == 'ZERO_RESULTS'){
                $cache->save(json_encode(array()), $key, array('geo_location_from_address'));
            }
        }
        return false;
    }

    public function addStoreIds(&$aRow)
    {
        $vStoreCode = isset($aRow['store_codes']) ? $aRow['store_codes'] : '';
        $vStoreIds = $this->getStoreIdFromCode($vStoreCode);
        $aRow['store_ids'] = $vStoreIds;
        // do not skip
        return false;
    }

    protected function getStoreIdFromCode($vStoreCode)
    {
        //default store code
        if (!$vStoreCode) {
            return '0';
        }
        if (isset($this->aCodeStoreIdMap[$vStoreCode])) {
            return $this->aCodeStoreIdMap[$vStoreCode];
        }
        $aStoreIds = array();
        $aCodeList = explode(',', $vStoreCode);
        $aStoreCodes = Mage::app()->getStores(false, true);
        foreach ($aCodeList as $vScopeCode) {
            $vScopeCode = trim($vScopeCode);
            /** @var Mage_Core_Model_Website $oWebsite */
            $oWebsite = Mage::getModel('core/website')->load($vScopeCode);
            if ($oWebsite->getId()) {
                $aWebsiteStoreIds = $oWebsite->getStoreIds();
                foreach ($aWebsiteStoreIds as $iStoreId) {
                    $aStoreIds[$iStoreId] = $iStoreId;
                }
            }
            else {
                if (isset($aStoreCodes[$vScopeCode])) {
                    $oStore = $aStoreCodes[$vScopeCode];
                    $aStoreIds[$oStore->getStoreId()] = $oStore->getStoreId();
                }
            }
        }
        $this->aCodeStoreIdMap[$vStoreCode] = implode(',', $aStoreIds);
        return $this->aCodeStoreIdMap[$vStoreCode];
    }
}