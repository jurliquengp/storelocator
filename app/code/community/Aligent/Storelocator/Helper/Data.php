<?php
/**
 * Data.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Helper_Data
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 *
 *
 * @todo: Need to DRY up the code in this class.
 */
class Aligent_Storelocator_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_STORE_TYPES = 'aligent_storelocator/settings/store_types';

    const CURRENT_STORELOCATION_REGISTRY_KEY = 'current_storelocation';

    public function getLocations($cLocations, $sPin = null, $sUnit = 'km') {

        $oResult          = new stdClass();
        $oResult->total   = 0;
        $oResult->results = array();

        foreach ($cLocations as $oLocation) {
            if ($sUnit == 'mi') {
                $oLocation->setDistance($oLocation->getDistance() * 0.621371192);
            }

            $oLoc = new stdClass();
            $oLoc->location = $oLocation->getLocation();
            foreach ($oLocation->getData() as $k => $v) {
                $oLoc->{preg_replace_callback('/_(.?)/', function($m) { return strtoupper($m[1]); }, $k)} = $v;
            }
            unset($oLoc->latitude);
            unset($oLoc->longitude);
            unset($oLoc->entity_id);

            if (isset($oLoc->storeType)) {
                $oLoc->storeType = explode(',', $oLoc->storeType);
            }

            $oLoc->id = $oLocation->getEntityId();

            $oLoc->zoom = 18;
            if ($sPin) {
                $oLoc->pin = $sPin;
            }
            $oLoc->color = '#000000';

            $oResult->results[] = $oLoc;
        }

        $oResult->total = count($cLocations);

        return $oResult;
    }

    public function search($fLatitude, $fLongitude, $iRadius, $sUnit = 'km', $bSearchLoc = true, $aStoreTypes = array(), $iPageSize = 0, $iPage = 0, $bReturnCollection = false)
    {
        if (!is_array($aStoreTypes)) {
            if ($aStoreTypes === 'All' || $aStoreTypes === null) {
                $aStoreTypes = array();
            } else {
                $aStoreTypes = array($aStoreTypes);
            }
        }

        $sPin = Mage::getStoreConfig('aligent_storelocator/settings/pin');

        $iCurrentStore = (int) Mage::app()->getStore()->getStoreId();

        $cLocations = Mage::getModel('aligent_storelocator/storelocation')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->filterByDistance($fLatitude, $fLongitude, $iRadius, $sUnit)
            ->filterByStoreType($aStoreTypes)
            ->setPageSize($iPageSize)
            ->setCurPage($iPage)
            ->addAttributeToFilter(array(
                array('attribute'=> 'store_ids','finset' => 0),
                array('attribute'=> 'store_ids','finset' => $iCurrentStore)
            ));

        $cLocations->getSelect()
            ->order('distance');

        Mage::dispatchEvent('store_locator_search_collection_load_before', array('collection' => $cLocations));

        if($bReturnCollection) {
            return $cLocations;
        }

        $cLocations->load();

        if ($bSearchLoc) {
            $oSearchLoc = Mage::getModel('aligent_storelocator/storelocation');
            $oSearchLoc->setId(0);
            $oSearchLoc->setName('Search Location');
            $oSearchLoc->setZoom(18);
            if ($sPin) {
                $oSearchLoc->setPin($sPin);
            }
            $location     = new stdClass();
            $location->la = $fLatitude;
            $location->lo = $fLongitude;
            $oSearchLoc->setLocation($location);
            $oSearchLoc->setShowInList(false);
            $oSearchLoc->setAlwaysShow(true);

            $cLocations->addItem($oSearchLoc);
        }

        $oResult = $this->getLocations($cLocations, $sPin, $sUnit);
        Mage::dispatchEvent('store_locator_search_after',array('locations' => $oResult));
        if ($bSearchLoc) {
            $oResult->total --;
        }

        return $oResult;
    }

    public function getAll()
    {
        $sPin = Mage::getStoreConfig('aligent_storelocator/settings/pin');

        $iCurrentStore = (int) Mage::app()->getStore()->getStoreId();

        $cLocations = Mage::getModel('aligent_storelocator/storelocation')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter(array(
                array('attribute'=> 'store_ids','finset' => 0),
                array('attribute'=> 'store_ids','finset' => $iCurrentStore)
            ));

        $cLocations->load();

        $oResult          = new stdClass();
        $oResult->total   = 0;
        $oResult->results = array();


        foreach ($cLocations as $oLocation) {
            $oLoc = new stdClass();
            $oLoc->location = $oLocation->getLocation();
            foreach ($oLocation->getData() as $k => $v) {
                $oLoc->{preg_replace_callback('/_(.?)/', function($m) { return strtoupper($m[1]); }, $k)} = $v;
            }
            unset($oLoc->latitude);
            unset($oLoc->longitude);
            unset($oLoc->entity_id);

            $oLoc->id = $oLocation->getEntityId();

            $oLoc->zoom = 18;
            if ($sPin) {
                $oLoc->pin = $sPin;
            }
            $oLoc->color = '#000000';

            $oResult->results[] = $oLoc;
        }

        $oResult->total = count($cLocations);

        return $oResult;
    }

    public function getStorelocationImageDir($filename = '') {
        return Mage::getBaseDir('media') . DS . 'storelocator_storelocation' . DS . $filename;
    }

    public function getStorelocationImageUrl($filename = '') {
        return Mage::getBaseUrl('media') . 'storelocator_storelocation/' . $filename;
    }

    /**
     * Reusable helper function to get list of store types for store locator.  This
     * helper was created to deals with the case where the config value is null
     * without crashing.
     *
     * @return array
     */
    public function getStoreTypes() {
        $sStoreTypeConfig = Mage::getStoreConfig('aligent_storelocator/settings/store_types');
        if (!$sStoreTypeConfig){
            return array();
        }
        $aStoreTypes = unserialize($sStoreTypeConfig);
        $aStoreTypes = $aStoreTypes ? $aStoreTypes : array();
        return $aStoreTypes;
    }


    /**
     * Helper to serialize and save the store type list.
     *
     * @param $aTypeList
     */
    public function saveStoreTypes($aTypeList) {
        Mage::getConfig()->saveConfig(self::XML_PATH_STORE_TYPES, serialize($aTypeList));
    }

}
