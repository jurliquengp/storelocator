<?php
/**
 * Geo.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Helper_Geo
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Helper_Geo extends Mage_Core_Helper_Abstract
{

    const LONG              = 'long';
    const NORMAL            = 'normal';
    const SHORT             = 'short';
    const EARTH_MEAN_RADIUS = 6372.797;

    const KM = 'kilometers';
    const MI = 'miles';

    protected $geocodeApi = 'https://maps.googleapis.com/maps/api/geocode/json';
    public $errorResponse = null;
    public $apiKey = null;

    public function getGeoLocationDistance(
        $fLocation1Latitude,
        $fLocation1Longitude,
        $fLocation2Latitude,
        $fLocation2Longitude,
        $sUnit = self::KM
    ) {
        $fPi80 = M_PI / 180;
        $fLocation1Latitude *= $fPi80;
        $fLocation1Longitude *= $fPi80;
        $fLocation2Latitude *= $fPi80;
        $fLocation2Longitude *= $fPi80;

        $fLatitudeDistance  = $fLocation2Latitude - $fLocation1Latitude;
        $fLongitudeDistance = $fLocation2Longitude - $fLocation1Longitude;
        $a                  = sin($fLatitudeDistance / 2) * sin($fLatitudeDistance / 2) + cos(
                $fLocation1Latitude
            ) * cos($fLocation2Latitude) * sin($fLongitudeDistance / 2) * sin($fLongitudeDistance / 2);
        $c                  = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $fKilometres        = self::EARTH_MEAN_RADIUS * $c;

        if ($sUnit == self::KM) {
            return $fKilometres;
        } else {
            return $fKilometres * 0.621371192;
        }
    }

    public function getGeolocationFromAddress($sAddress, $bSensor = false)
    {
        $oResult = $this->getGeocodeFromAddress($sAddress, $bSensor);
        if (!$oResult && $this->errorResponse == 'OVER_QUERY_LIMIT'){
            sleep(1);
            $oResult = $this->getGeocodeFromAddress($sAddress, $bSensor);
        }
        if ($oResult) {
            return $oResult->geometry->location;
        }
    }

    public function getFormattedAddressFromAddress($sAddress, $sFormat = self::NORMAL, $bSensor = false)
    {
        $oResult = $this->getGeocodeFromAddress($sAddress, $bSensor);
        if (!$oResult) {
            return false;
        }

        switch ($sFormat) {
            case self::NORMAL:
                return $oResult->formatted_address;
                break;
            case self::LONG:
                $sResponse = '';
                foreach ($oResult->address_components as $oComponent) {
                    $sResponse .= $oComponent->long_name . ' ';
                }

                return trim($sResponse);
                break;
            case self::SHORT:
                $sResponse = '';
                foreach ($oResult->address_components as $oComponent) {
                    $sResponse .= $oComponent->short_name . ' ';
                }

                return trim($sResponse);
                break;
        }
    }

    /**
     * @private - Don't expose the raw results to client code.
     *
     * @param      $sAddress
     * @param bool $bSensor
     *
     * @return bool
     */
    protected function getGeocodeFromAddress($sAddress, $bSensor = false)
    {
        $sRequest = $this->geocodeApi . '?address=' . urlencode($sAddress) . '&sensor=' . urlencode($bSensor) . '&key=' . urlencode($this->getApiKey());
        $oResponse = Zend_Json::decode(file_get_contents($sRequest),Zend_Json::TYPE_OBJECT);
        if (empty($oResponse->results[0])) {
            if (empty($oResponse->status)){
                $this->errorResponse = 'Google Api did not return any status';
            }
            else{
                $this->errorResponse = $oResponse->status;
            }
            return false;
        } else {
            return $oResponse->results[0];
        }
    }
    public function getApiKey()
    {
        if ($this->apiKey!==null){
            return $this->apiKey;
        }
        return ($this->apiKey = Mage::getStoreConfig('aligent_storelocator/google_api/maps_api_key'));
    }

}
