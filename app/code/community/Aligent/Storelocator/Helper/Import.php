<?php
/**
 * Aligent_Storelocator_Helper_Import
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Zain <zain@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Helper_Import extends Mage_Core_Helper_Abstract {

    public function addStoreTypes($typesToAdd) {
        $typeList = Mage::helper('aligent_storelocator')->getStoreTypes();
        $storeTypes = $this->_getStoreTypes($typeList);
        foreach ($typesToAdd as $typeName) {
            if (isset($storeTypes[$typeName]))
                continue;
            $key = (string)microtime(true);
            $key = '_' . str_replace('.', '_', $key) . '_' . mt_rand();
            $typeList[$key] = array('store_type' => $typeName);
        }
        Mage::helper('aligent_storelocator')->saveStoreTypes($typeList);
    }

    protected function _getStoreTypes($typeList) {
        $storeTypes = array();
        foreach ($typeList as $typeArray) {
            if (empty($typeArray['store_type']))
                continue;
            $storeTypes[$typeArray['store_type']] = $typeArray['store_type'];
        }
        return $storeTypes;
    }
}
