<?php
/**
 * IndexController.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_IndexController
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function searchAction()
    {
        $oResponse = $this->getResponse();
        $oRequest  = $this->getRequest();

        $iRadius    = Mage::helper('core')->escapeHtml($oRequest->get('radius'));
        $sUnit      = Mage::helper('core')->escapeHtml($oRequest->get('unit'));
        $fLatitude  = (float) Mage::helper('core')->escapeHtml($oRequest->get('latitude'));
        $fLongitude = (float) Mage::helper('core')->escapeHtml($oRequest->get('longitude'));
        $bShowSearchPin = (boolean) Mage::getStoreConfig('aligent_storelocator/settings/search_pin');
        $aStoreTypes = Mage::helper('core')->escapeHtml($oRequest->get('store_type'));
        $iPageSize  = intval(Mage::helper('core')->escapeHtml($oRequest->get('page_size')));
        $iPage  = intval(Mage::helper('core')->escapeHtml($oRequest->get('page')));

        $oResult = Mage::app()->getHelper('aligent_storelocator/data')
            ->search($fLatitude, $fLongitude, $iRadius, $sUnit, $bShowSearchPin, $aStoreTypes, $iPageSize, $iPage);

        $oResponse->setBody(Mage::helper('core')->jsonEncode($oResult));
        $oResponse->setHeader('Content-type', 'application/json');
    }
}
