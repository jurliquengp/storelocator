<?php
/**
 * StoreController.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_StoreController
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_StoreController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $storeId = $this->getRequest()->getparam('id');

        if (!is_null($storeId)) {
            Mage::register(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY, $storeId);
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    public function locatorAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

}
