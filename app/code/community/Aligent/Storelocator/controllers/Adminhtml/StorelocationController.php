<?php
/**
 * StorelocationController.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Adminhtml_StorelocationController
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Adminhtml_StorelocationController extends Mage_Adminhtml_Controller_Action
{

    /**
     * @param string $idFieldName
     *
     * @return Aligent_Storelocator_Model_Storelocation
     */
    protected function _initStorelocation($idFieldName = 'id')
    {
        $this->_title($this->__('Store Locator'))->_title($this->__('Manage Store Locations'));

        $storeLocationId = (int) $this->getRequest()->getParam($idFieldName);
        $storeLocation   = Mage::getModel('aligent_storelocator/storelocation');

        if ($storeLocationId) {
            $storeLocation->load($storeLocationId);
        }

        Mage::register(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY, $storeLocation);

        return $storeLocation;
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->_title($this->__('Store Locator'))->_title($this->__('Manage Store Locations'));

        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
             ->_setActiveMenu('aligent_storelocator/storelocation')
             ->_addBreadcrumb(
             Mage::helper('aligent_storelocator')->__('Store Locator'),
             Mage::helper('cms')->__('Store Locator')
            )
             ->_addBreadcrumb(
             Mage::helper('cms')->__('Manage Store Locations'),
             Mage::helper('cms')->__('Manage Store Locations')
            );
        $this->renderLayout();
    }

    /**
     * Storelocation edit action
     */
    public function editAction()
    {
        $storeLocation = $this->_initStorelocation();
        $this->loadLayout();

        $this->_title($storeLocation->getId() ? $storeLocation->getName() : $this->__('New Store Location'));

        $data = Mage::getSingleton('adminhtml/session')->getStorelocatorData(true);
        if (!empty($data)) {
            $storeLocation->addData($data);
        }

        /**
         * Set active menu item
         */
        $this->_setActiveMenu('aligent_storelocator/storelocation');

        $block = $this->getLayout()->getBlock('catalog.wysiwyg.js');
        if ($block) {
            $block->setStoreId($storeLocation->getStoreId());
        }

        $this->renderLayout();
    }

    /**
     * Create new store action
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {

        $data = $this->getRequest()->getPost();
        if (!$data) {
            goto endSaveAction;
        }

        $session       = Mage::getSingleton('adminhtml/session');
        $redirectBack  = $this->getRequest()->getParam('back', false);
        $storeLocation = $this->_initStorelocation('storelocation_id');

        /**
         * @var $storeLocationForm Mage_Eav_Model_Form
         */
        $storeLocationForm = Mage::getModel('aligent_storelocator/form');
        $storeLocationForm->setEntity($storeLocation)
                          ->setFormCode('adminhtml_storelocation')
                          ->ignoreInvisible(false);


        $formData = $storeLocationForm->extractData($this->getRequest(), 'store_information');

        $errors = $storeLocationForm->validateData($formData);
        if ($errors != true) {
            foreach ($errors as $error) {
                $session->addError($error);
            }
            $session->setStorelocationData($data);
            $this->_redirect('*/*/edit', array('id' => $storeLocation->getId()));

            return;
        }

        $storeLocationForm->compactData($formData);

        try {
            $storeLocation->save();

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $storeLocation->getId(), '_current' => true));

                return;
            }
        } catch (Exception $e) {
            $session->addError($e->getMessage());
            $session->setStorelocationData($data);
            $this->_redirect('*/*/edit', array('id' => $storeLocation->getId()));

            return;
        }

        endSaveAction:
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        $storeLocation = $this->_initStorelocation();
        if ($storeLocation->getId()) {
            try {
                $storeLocation->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('aligent_storelocator')->__('The store location has been deleted.')
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/');
    }

    public function validateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);

        // @todo: add appropriate validation conditions here...
        if (false) { // Add validation conditions
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('aligent_storelocator')->__('There was an error validating the store location')
            );
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }

        $this->getResponse()->setBody($response->toJson());
    }

    public function geolocateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);

        try {
            $address = trim($this->getRequest()->getParam('address'));
            if (strlen($address) < 1) {
                throw new Exception('Address was not supplied');
            }
            $location = Mage::helper('aligent_storelocator/geo')->getGeolocationFromAddress($address);
            $response->setLocation($location);
        } catch (Exception $e) {
            Mage::logException($e);
            $response->setError(true);
            $response->setMessage(sprintf('An error occured: "%s"', $e->getMessage()));
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($response->toJson());
    }

    /**
     * WYSIWYG editor action for ajax request
     *
     */
    public function wysiwygAction()
    {
        $elementId     = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId       = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock(
                        'adminhtml/catalog_helper_form_wysiwyg_content',
                        '',
                        array(
                            'editor_element_id' => $elementId,
                            'store_id'          => $storeId,
                            'store_media_url'   => $storeMediaUrl,
                        )
        );
        $this->getResponse()->setBody($content->toHtml());
    }

    protected function _isAllowed(){
        return Mage::getSingleton('admin/session')->isAllowed('aligent_storelocator/storelocation');
    }

    /**
     * Export stockists to CSV format
     */
    public function exportCsvAction()
    {
        $vFileName = 'stockists.csv';
        $oCsvBlock = $this->getLayout()->createBlock('aligent_storelocator/adminhtml_storelocation_grid');
        $oCsvBlock->addColumnAfter(
                            'phone',
                            array(
                                'header' => Mage::helper('aligent_storelocator')->__('Phone'),
                                'align'  => 'left',
                                'index'  => 'phone',
                            ),'postcode'
                        )
                    ->addColumnAfter(
                            'address1',
                            array(
                                'header' => Mage::helper('aligent_storelocator')->__('Address'),
                                'align'  => 'left',
                                'index'  => 'address1',
                            ),'name'
                        );
        $this->_prepareDownloadResponse($vFileName, $oCsvBlock->getCsvFile());
    }

}
