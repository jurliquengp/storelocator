<?php
/** @var Aligent_Storelocator_Model_Entity_Setup $installer */
$installer = $this;

$installer->startSetup();

$installer->setConfigData('aligent_storelocator/settings/search_pin', 1);

$installer->endSetup();
