<?php

/** @var Aligent_Storelocator_Model_Entity_Setup $setup */
$setup = $this;
$setup->startSetup();

$vType = 'storelocator_storelocation';
$vAttrName = 'open_hours';

//Need to delete existing attribute values.
$attrId = $setup->getAttribute($vType, $vAttrName, 'attribute_id');
$backendTable = $setup->getTable(array('aligent_storelocator/storelocations', 'varchar'));

$setup->getConnection()->delete($backendTable, array('attribute_id = ?' => $attrId));

$setup->updateAttribute($vType, $vAttrName, array(
    'frontend_input'            => 'opening_hours',
    'backend_model'             => 'eav/entity_attribute_backend_serialized',
    'backend_type'              => 'text'
));

$setup->endSetup();