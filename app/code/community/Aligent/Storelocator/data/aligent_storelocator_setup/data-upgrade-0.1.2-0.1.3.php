<?php
/**
 * data-upgrade-0.1.2-0.1.3.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/** @var Aligent_Storelocator_Model_Entity_Setup $installer */
$installer = $this;

$installer->startSetup();

$installer->setConfigData('aligent_storelocator/google_api/maps_api_url', 'http://maps.googleapis.com/maps/api/js');

$installer->endSetup();