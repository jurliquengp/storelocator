<?php

/**
 * Class Aligent_Storelocator_Model_Attribute_Data_Openinghours
 *
 * DataModel implementation required by the EAV form extractor/compactor
 * Can be extended to provide additional validation/processing on the value during the save step,
 * but serialization happens as part of the backend_model so no real work is done here.
 */
class Aligent_Storelocator_Model_Attribute_Data_Openinghours extends Mage_Eav_Model_Attribute_Data_Abstract
{
    public function restoreValue($value)
    {
        return $value;
    }

    public function compactValue($value)
    {
        $this->getEntity()->setData($this->getAttribute()->getAttributeCode(), $value);
        return $this;
    }

    public function outputValue($format = Mage_Eav_Model_Attribute_Data::OUTPUT_FORMAT_TEXT)
    {
        $value = $this->getEntity()->getData($this->getAttribute()->getAttributeCode());
        return $value;
    }

    public function extractValue(Zend_Controller_Request_Http $request)
    {
        $value = $this->_getRequestValue($request);
        return $value;
    }

    public function validateValue($value)
    {
        return true;
    }
}