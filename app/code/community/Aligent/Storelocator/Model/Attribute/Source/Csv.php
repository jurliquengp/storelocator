<?php
/**
 * Image.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Model_Storelocation_Attribute_Backend_Image
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Model_Attribute_Source_Csv extends Mage_Core_Model_Config_Data
{
    public function __construct()
    {
        parent::_construct();
        $importHelper= Mage::helper('aligent_storelocator/csvImport');

        //should not be needed but _afterSave not working
//        $this->ImportIfPosted();
    }

    public function ImportIfPosted()
    {
        static $saved = false;
        if ($saved)
            return;
        if (empty($_FILES)){
            return;
        }

        if (empty($_FILES['groups']['tmp_name']['settings']['fields']['import']['value'])) {
            return ;
        }
        $tempFile = $_FILES['groups']['tmp_name']['settings']['fields']['import']['value'];

        /** @var $importHelper Aligent_Storelocator_Helper_CsvImport */
        $importHelper= Mage::helper('aligent_storelocator/csvImport');
        $importHelper->dataMap['combined']['csvPath'] = $tempFile;
        $importHelper->skipCheckFunction['addLatitudeLongitude'] = 'addLatitudeLongitude';
        $importHelper->skipCheckFunction['addStoreIds'] = 'addStoreIds';
        $importHelper->importAll();
        $saved = true;

    }

    public function _afterSave()
    {
        $this->ImportIfPosted();
    }
}
