<?php

class Aligent_Storelocator_Model_Attribute_Source_Stores extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm();
            array_unshift($this->_options, array('value' => '0', 'label' => Mage::helper('customer')->__('All Stores')));
        }
        return $this->_options;
    }

    public function getOptionText($value)
    {
        $isMultiple = false;
        if (strpos($value, ',')) {
            $isMultiple = true;
            $value = explode(',', $value);
        }

        if (!$this->_options) {
            $collection = Mage::getResourceModel('core/store_collection');
            $this->_options = $collection->load()->toOptionArray();
        }

        if ($isMultiple) {
            $values = array();
            foreach ($value as $val) {
                $values[] = $this->_options[$val];
            }
            return $values;
        }
        else {
            return $this->_options[$value];
        }
        return false;
    }
}
