<?php

class Aligent_Storelocator_Model_Attribute_Source_Storetypes extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    public function getAllOptions()
    {
        if (!$this->_options) {
            $aStoreTypes = Mage::helper('aligent_storelocator')->getStoreTypes();

            $this->_options = array();
            foreach ($aStoreTypes as $aStoreType) {
                $this->_options[] = array('label' => $aStoreType['store_type'], 'value' => $aStoreType['store_type']);
            }
        }
        return $this->_options;
    }

}
