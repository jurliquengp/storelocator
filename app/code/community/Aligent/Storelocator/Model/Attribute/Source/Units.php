<?php

class Aligent_Storelocator_Model_Attribute_Source_Units extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = array(
                array(
                    'label' => 'Kilometers',
                    'value' => 'km'
                ),
                array(
                    'label' => 'Miles',
                    'value' => 'mi'
                )
            );
        }
        return $this->_options;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
