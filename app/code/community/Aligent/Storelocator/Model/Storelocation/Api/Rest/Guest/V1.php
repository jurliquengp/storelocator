<?php

/**
 * Class Aligent_Storelocator_Model_Storelocation_Api_Rest_Customer_V1
 *
 * This class must exist due to the convention of the API Dispatcher ( \Mage_Api2_Model_Dispatcher::loadResourceModel )
 * It programatically builds the class name, so it must be Module/Resource/ApiType/UserType/Version.
 */
class Aligent_Storelocator_Model_Storelocation_Api_Rest_Guest_V1 extends Aligent_Storelocator_Model_Storelocation_Api_Rest_Abstract
{}