<?php

class Aligent_Storelocator_Model_Storelocation_Api_Rest_Abstract extends Mage_Api2_Model_Resource
{
    public $_aIncludedAttributes = Array(
        'name',
        'country',
        'city' ,
        'address1',
        'state',
        'postcode',
        'latitude',
        'longitude',
        'locator_id',
        'phone',
        'website'
    );

    /**
     * An overriding class can set an attribute map, which allows you to set a different name to be presented
     * in the JSON for each attribute.
     *
     * Eg:
     * $_aAttributeMap = Array(
     *  'latitude' => 'lat',
     *  'phone' => 'phone_number'
     * )
     */
    public $_aAttributeMap = Array();

    protected function _startRequest() {
        Mage::dispatchEvent('aligent_storelocator_api_request_before', Array('requestModel' => $this));
        $oStoreScope = $this->_getStoreScope();

        if($oStoreScope && ($oStoreScope->getId() || $oStoreScope->getId() === 0)) {
            Mage::app()->setCurrentStore($oStoreScope);
        }
    }

    protected function _endRequest() {
        Mage::dispatchEvent('aligent_storelocator_api_request_after', Array('requestModel' => $this));
    }

    public function _getStoreScope() {
        /** @var $oCustomer Mage_Customer_Model_Customer */
        $oCustomer = $this->_getAuthedCustomer();
        if(!$oCustomer) return false;

        $oStore = $oCustomer->getStore();

        return $oStore;
    }

    public function _getAuthedCustomer() {
        try {
            $oApiUser = $this->getApiUser();
            $iApiUserId = $oApiUser->getUserId();
        } catch(Exception $e) {
            /* getApiUser() will throw an exception if no user is found, so
             * we catch it here as it isn't really an exceptional
             * application state and we are able to continue without it. */
            return false;
        }

        return Mage::getModel('customer/customer')->load($this->getApiUser()->getUserId());
    }

    public function _retrieve () {
        $this->_startRequest();
        $iEntityId = (int) $this->getRequest()->getParam('entity_id');

        if(!$iEntityId) {
            $this->_endRequest();
            return false;
        }

        $oStoreLocation = Mage::getModel('aligent_storelocator/storelocation')->load($iEntityId);
        $vReturnJson = json_encode($this->_getLocationdata($oStoreLocation));

        $this->_endRequest();
        return $vReturnJson;
    }

    public function _getLocationData($oLocation) {
        $aRoot = Array();
        $oLocation->load();

        foreach($this->_aIncludedAttributes as $vAttributeCode) {

            if(isset($this->_aAttributeMap[$vAttributeCode])) {
                $vFrontAttributeCode = $this->_aAttributeMap[$vAttributeCode];
            } else {
                $vFrontAttributeCode = $vAttributeCode;
            }

            $aRoot[$vFrontAttributeCode] = ($vData = $oLocation->getData($vAttributeCode)) ? $vData : '';
        }

        return $aRoot;
    }

    public function _retrieveCollection () {
        $this->_startRequest();
        $oRequest = $this->getRequest();

        if($oRequest->getParam('lat') && $oRequest->getParam('lng')) {

            $iMaxRadius = (int)Mage::getStoreConfig('aligent_storelocator/settings/max_radius');
            $iRequestedRadius = (int)Mage::helper('core')->escapeHtml($oRequest->getParam('radius'));

            /* If the requested radius is zero, the radius SQL will instead return all results,
             * so we must send it some radius above zero as not to expose all results */
            if ($iRequestedRadius === 0) {
                $iUsedRadius = $iMaxRadius;
            } else {
                $iUsedRadius = min($iMaxRadius, $iRequestedRadius);
            }

            /** @var $oCollection  Aligent_Storelocator_Model_Resource_Storelocation_Collection */
            $oCollection = $this->_search(
                Mage::helper('core')->escapeHtml($oRequest->getParam('lat')),
                Mage::helper('core')->escapeHtml($oRequest->getParam('lng')),
                $iUsedRadius
            );

            //TODO: Replace with store config value to configure a limit
            $oCollection->getSelect()->limit(10);

            $aLocations = Array();

            foreach($oCollection as $oLocation) {
                $aLocations[] = $this->_getLocationData($oLocation);
            }

            $this->_endRequest();
            return json_encode($aLocations);

        } else {
            return Array();
        }
    }

    public function _search ($fLat, $fLng, $iRadius) {
        $oHelper = Mage::app()->getHelper('aligent_storelocator/data');
        $oCollection = $oHelper->search(
            $fLat, $fLng, $iRadius,
            $sUnit = 'km',
            $bSearchLoc = false,
            $aStoreTypes = array(),
            $iPageSize = 0, $iPage = 0,
            true
        );

        return $oCollection;
    }
}