<?php
/**
 * Storelocation.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Model_Resource_Storelocation
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Model_Resource_Storelocation extends Mage_Eav_Model_Entity_Abstract
{

    public function _construct()
    {
        $resource = Mage::getSingleton('core/resource');
        $this->setType('storelocator_storelocation');
        $this->setConnection(
             $resource->getConnection('storelocator_read'),
             $resource->getConnection('storelocator_write')
        );
    }

    protected function _getDefaultAttributes()
    {
        return array_merge(parent::_getDefaultAttributes(), array('store_id', 'is_active'));
    }

}
