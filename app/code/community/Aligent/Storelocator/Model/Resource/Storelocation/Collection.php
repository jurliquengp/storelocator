<?php
/**
 * Collection.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Model_Resource_Storelocation_Collection
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Model_Resource_Storelocation_Collection extends Mage_Eav_Model_Entity_Collection
{

    const EARTH_MEAN_RADIUS_KM = 6372.797;

    protected function _construct()
    {
        $this->_init('aligent_storelocator/storelocation');
    }

    public function filterByDistance($fLatitude, $fLongitude, $iRadius = null, $sUnit = 'km')
    {

        if ($sUnit == 'mi') {
            $iRadius *= 1.609344; // convert to km
        }

        $this->addExpressionAttributeToSelect(
            'distance',
            '(' . self::EARTH_MEAN_RADIUS_KM . ' * ACOS('
            . 'COS(RADIANS(' . (float)$fLatitude . ')) * '
            . 'COS(RADIANS({{latitude}})) * '
            . 'COS(RADIANS({{longitude}}) - RADIANS(' . (float)$fLongitude . ')) + '
            . 'SIN(RADIANS(' . (float)$fLatitude . ')) * '
            . 'SIN(RADIANS({{latitude}}))'
            . '))',
            array(
                'latitude',
                'longitude'
            )
        );

        if ($iRadius) {
            $this->getSelect()
                ->having('distance <= ?', (int)$iRadius);
        }

        return $this;
    }

    public function filterByStoreType($aStoreTypes) {
        if (!empty($aStoreTypes)) {
            $filters = array();
            foreach ($aStoreTypes as $storeType) {
                $filters[] = array('attribute' => 'store_type', 'finset' => $storeType);
            }
            $this->addAttributeToFilter($filters);
        }
        return $this;
    }
    public function setSelect(Zend_Db_Select $oSelect)
    {
        $this->_select = $oSelect;
    }
}
