<?php

class Aligent_Storelocator_Model_Resource_Searchlog extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('aligent_storelocator/searchlog');
    }

    public function getSearchCount($model, $ip, $startTime) {
        try{
            $select = $this->_getReadAdapter()->select()
                ->from(array('main_table' => $this->getTable('aligent_storelocator/searchlog')), new Zend_Db_Expr('count(*)'))
                ->where('main_table.ip = ?',  $ip)
                ->where('main_table.time >= ?', $startTime);

            $data = $this->_getReadAdapter()->fetchRow($select);

            return $data['count(*)'];
        }
        catch ( Exception $e){
            return 150;
        }
    }

    public function deleteLogsBefore($time) {
        $deleted = $this->_getWriteAdapter()
            ->delete($this->getTable('aligent_storelocator/searchlog'), $this->_getWriteAdapter()->quoteInto('time < ?', $time));

        return $this;
    }
}