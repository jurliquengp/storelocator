<?php
/**
 * Attribute.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Model_Resource_Form_Attribute
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Model_Resource_Form_Attribute extends Mage_Eav_Model_Resource_Form_Attribute
{

    /**
     * Initialize connection and define main table
     *
     */
    protected function _construct()
    {
        $this->_init('aligent_storelocator/form_attribute', 'attribute_id');
    }
}
