<?php
/**
 * Storelocation.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Model_Storelocation
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 *
 * @method string getName()
 * @method string getAddress1()
 * @method string getAddress2()
 * @method string getCity()
 * @method string getState()
 * @method string getPostcode()
 * @method string getDescription()
 */
class Aligent_Storelocator_Model_Storelocation extends Mage_Core_Model_Abstract
{

    protected $_eventPrefix = 'aligent_storelocation';
    protected $_eventObject = 'store_location';

    protected function _construct()
    {
        $this->_init('aligent_storelocator/storelocation');
    }

    public function getLocation()
    {
        if (parent::getLocation() === null) {
            return array('la' => $this->getLatitude(), 'lo' => $this->getLongitude());
        } else {
            return parent::getLocation();
        }
    }

}
