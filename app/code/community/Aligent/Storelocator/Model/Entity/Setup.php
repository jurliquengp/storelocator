<?php
/**
 * Setup.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Model_Entity_Setup
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Model_Entity_Setup extends Mage_Eav_Model_Entity_Setup
{

    public function getDefaultEntities()
    {
        return array(
            'storelocator_storelocation' => array(
                'entity_model'               => 'aligent_storelocator/storelocation',
                'attribute_model'            => 'aligent_storelocator/attribute',
                'table'                      => 'aligent_storelocator/storelocations',
                'additional_attribute_table' => 'aligent_storelocator/eav_attribute',
                'attributes'                 => array(
                    'name'      => array(
                        //the EAV attribute 'type' type, NOT a mysql varchar
                        'type'             => 'varchar',
                        'backend'          => '',
                        'frontend'         => '',
                        'label'            => 'Name',
                        'input'            => 'text',
                        'class'            => '',
                        'source'           => '',
                        // store scope == 0
                        // global scope == 1
                        // website scope == 2
                        'global'           => 0,
                        'visible'          => true,
                        'required'         => true,
                        'user_defined'     => true,
                        'default'          => '',
                        'searchable'       => true,
                        'filterable'       => true,
                        'comparable'       => false,
                        'visible_on_front' => true,
                        'unique'           => false,
                        'sort_order'       => 10,
                    ),
                    'address1'  => array(
                        //the EAV attribute 'type' type, NOT a mysql varchar
                        'type'             => 'varchar',
                        'backend'          => '',
                        'frontend'         => '',
                        'label'            => 'Address Line 1',
                        'input'            => 'text',
                        'class'            => '',
                        'source'           => '',
                        // store scope == 0
                        // global scope == 1
                        // website scope == 2
                        'global'           => 0,
                        'visible'          => true,
                        'required'         => false,
                        'user_defined'     => true,
                        'default'          => '',
                        'searchable'       => false,
                        'filterable'       => false,
                        'comparable'       => false,
                        'visible_on_front' => true,
                        'unique'           => false,
                        'sort_order'       => 20,
                    ),
                    'address2'  => array(
                        //the EAV attribute 'type' type, NOT a mysql varchar
                        'type'             => 'varchar',
                        'backend'          => '',
                        'frontend'         => '',
                        'label'            => 'Address Line 2',
                        'input'            => 'text',
                        'class'            => '',
                        'source'           => '',
                        // store scope == 0
                        // global scope == 1
                        // website scope == 2
                        'global'           => 0,
                        'visible'          => true,
                        'required'         => false,
                        'user_defined'     => true,
                        'default'          => '',
                        'searchable'       => false,
                        'filterable'       => false,
                        'comparable'       => false,
                        'visible_on_front' => true,
                        'unique'           => false,
                        'sort_order'       => 30,
                    ),
                    'city'      => array(
                        //the EAV attribute 'type' type, NOT a mysql varchar
                        'type'             => 'varchar',
                        'backend'          => '',
                        'frontend'         => '',
                        'label'            => 'City',
                        'input'            => 'text',
                        'class'            => '',
                        'source'           => '',
                        // store scope == 0
                        // global scope == 1
                        // website scope == 2
                        'global'           => 0,
                        'visible'          => true,
                        'required'         => false,
                        'user_defined'     => true,
                        'default'          => '',
                        'searchable'       => true,
                        'filterable'       => true,
                        'comparable'       => false,
                        'visible_on_front' => true,
                        'unique'           => false,
                        'sort_order'       => 40,
                    ),
                    'state'     => array(
                        //the EAV attribute 'type' type, NOT a mysql varchar
                        'type'             => 'varchar',
                        'backend'          => '',
                        'frontend'         => '',
                        'label'            => 'State',
                        'input'            => 'text',
                        'class'            => '',
                        'source'           => '',
                        // store scope == 0
                        // global scope == 1
                        // website scope == 2
                        'global'           => 0,
                        'visible'          => true,
                        'required'         => false,
                        'user_defined'     => true,
                        'default'          => '',
                        'searchable'       => true,
                        'filterable'       => true,
                        'comparable'       => false,
                        'visible_on_front' => true,
                        'unique'           => false,
                        'sort_order'       => 50,
                    ),
                    'postcode'  => array(
                        //the EAV attribute 'type' type, NOT a mysql varchar
                        'type'             => 'varchar',
                        'backend'          => '',
                        'frontend'         => '',
                        'label'            => 'Post Code',
                        'input'            => 'text',
                        'class'            => '',
                        'source'           => '',
                        // store scope == 0
                        // global scope == 1
                        // website scope == 2
                        'global'           => 0,
                        'visible'          => true,
                        'required'         => false,
                        'user_defined'     => true,
                        'default'          => '',
                        'searchable'       => true,
                        'filterable'       => true,
                        'comparable'       => false,
                        'visible_on_front' => true,
                        'unique'           => false,
                        'sort_order'       => 60,
                    ),
                    'latitude'  => array(
                        //the EAV attribute 'type' type, NOT a mysql varchar
                        'type'             => 'latlong',
                        'backend'          => '',
                        'frontend'         => '',
                        'label'            => 'Latitude',
                        'input'            => 'text',
                        'class'            => '',
                        'source'           => '',
                        // store scope == 0
                        // global scope == 1
                        // website scope == 2
                        'global'           => 0,
                        'visible'          => true,
                        'required'         => false,
                        'user_defined'     => true,
                        'default'          => '',
                        'searchable'       => false,
                        'filterable'       => false,
                        'comparable'       => false,
                        'visible_on_front' => true,
                        'unique'           => false,
                        'sort_order'       => 70,
                    ),
                    'longitude' => array(
                        //the EAV attribute 'type' type, NOT a mysql varchar
                        'type'             => 'latlong',
                        'backend'          => '',
                        'frontend'         => '',
                        'label'            => 'Longitude',
                        'input'            => 'text',
                        'class'            => '',
                        'source'           => '',
                        // store scope == 0
                        // global scope == 1
                        // website scope == 2
                        'global'           => 0,
                        'visible'          => true,
                        'required'         => false,
                        'user_defined'     => true,
                        'default'          => '',
                        'searchable'       => false,
                        'filterable'       => false,
                        'comparable'       => false,
                        'visible_on_front' => true,
                        'unique'           => false,
                        'sort_order'       => 80,
                    ),
                )
            )
        );
    }

    public function createEntityTables($baseTableName, array $options = array())
    {
        parent::createEntityTables($baseTableName, $options);
        $this->addEntityTablesUniqueConstraints($baseTableName, $options);

        return $this;
    }

    public function addEntityTablesUniqueConstraints($baseTableName, array $options = array())
    {
        $connection = $this->getConnection();

        //// Begin copy code from \Mage_Eav_Model_Entity_Setup::createEntityTables to get the eav table names etc.
        //
        $isNoDefaultTypes = $this->_getValue($options, 'no-default-types', false);
        $customTypes      = $this->_getValue($options, 'types', array());

        $types = array();
        if (!$isNoDefaultTypes) {
            $types = array(
                'datetime' => array(Varien_Db_Ddl_Table::TYPE_DATETIME, null),
                'decimal'  => array(Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4'),
                'int'      => array(Varien_Db_Ddl_Table::TYPE_INTEGER, null),
                'text'     => array(Varien_Db_Ddl_Table::TYPE_TEXT, '64k'),
                'varchar'  => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255'),
                'char'     => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255')
            );
        }

        if (!empty($customTypes)) {
            foreach ($customTypes as $type => $fieldType) {
                if (count($fieldType) != 2) {
                    throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('Wrong type definition for %s', $type));
                }
                $types[$type] = $fieldType;
            }
        }
        //
        //// End copy from \Mage_Eav_Model_Entity_Setup::createEntityTables.

        $uniqueIndexFields = array(
            'attribute_id',
            'entity_id',
            'store_id'
        );

        $uniqueIndexType = Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE;

        try {
            foreach ($types as $type => $fieldType) {
                $eavTableName = array($baseTableName, $type);
                $tableName    = $this->getTable($eavTableName);

                $connection->addIndex(
                           $tableName,
                           $this->getIdxName(
                                $tableName,
                                $uniqueIndexFields,
                                $uniqueIndexType
                           ),
                           $uniqueIndexFields,
                           $uniqueIndexType
                );
            }

        } catch (Exception $e) {
            throw Mage::exception(
                      'Mage_Eav',
                      Mage::helper('aligent_storelocator')->__('Can\'t add unique index to table: %s', $tableName)
            );
        }

        return $this;
    }

    /**
     * Prepare catalog attribute values to save
     *
     * @param array $attr
     *
     * @return array
     */
    protected function _prepareValues($attr)
    {
        $data = parent::_prepareValues($attr);
        $data = array_merge(
            $data,
            array(
                'is_wysiwyg_enabled'       => $this->_getValue($attr, 'wysiwyg_enabled', 0),
                'is_html_allowed_on_front' => $this->_getValue($attr, 'is_html_allowed_on_front', 0),
            )
        );

        return $data;
    }
}
