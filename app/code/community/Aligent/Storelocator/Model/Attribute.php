<?php
/**
 * Attribute.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Model_Attribute
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Model_Attribute extends Mage_Eav_Model_Attribute
{
    /**
     * Name of the module
     */
    const MODULE_NAME = 'Aligent_Storelocator';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'storelocator_entity_attribute';

    /**
     * Prefix of model events object
     *
     * @var string
     */
    protected $_eventObject = 'attribute';

    /**
     * Init resource model
     */
    protected function _construct()
    {
        $this->_init('aligent_storelocator/attribute');
    }
}
