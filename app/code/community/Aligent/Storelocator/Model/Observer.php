<?php

class Aligent_Storelocator_Model_Observer
{
    public function onStoreLocatorSearch($oObserver)
    {
        $searchLogModel = Mage::getModel('aligent_storelocator/searchlog');
        $searchLogModel->setIp(Mage::helper('core/http')->getRemoteAddr());
        $maxSearchesExceeded = $searchLogModel->getMaxSearchesExceeded();

        if($maxSearchesExceeded){
            $oResponse = Mage::app()->getResponse();
            $oResult = new stdClass;
            $oResult->markers = array();
            $oResult->pager_html = '';
            $oResult->error = $searchLogModel->getLimitErrorMessage();
            $oResponse->setBody(Mage::helper('core')->jsonEncode($oResult));
            $oResponse->setHeader('Content-type', 'application/json');
            $oResponse->setHttpResponseCode(429);
            $oResponse->sendResponse();
            exit();
        }
    }
}