<?php

/**
 * @package    Aligent_Storelocator
 * @author     Jonathan Day <jonathan@aligent.com.au>
 */

class Aligent_Storelocator_Model_Searchlog extends Mage_Core_Model_Abstract
{
    const DEFAULT_LIMIT_ERROR_MESSAGE = 'You have reached your request limit.';
    protected $_ip = 0;
    protected $_period = 3600; // hour

    protected function _construct()
    {
        $this->_init('aligent_storelocator/searchlog');
    }

    public function getLimitErrorMessage(){
        $vMessage = self::DEFAULT_LIMIT_ERROR_MESSAGE;
        $vConfigMessage = Mage::getStoreConfig('aligent_storelocator/general/limit_exceed_message');

        $vFinalMessage = ($vConfigMessage) ? $vConfigMessage : $vMessage;
        $vTranslatedMessage = Mage::helper('core')->__($vFinalMessage);

        return $vTranslatedMessage;
    }

    /**
     * Check if current visitor has exceeded allowed number of searches
     *
     * @return boolean
     */
    public function getMaxSearchesExceeded()
    {
        $amount =  $this->_searchesByUser();

        if ($this->getMaxSearches() == 0){  //if config value for max is zero, allow unlimited
            return false;
        }
        else if ($amount >= $this->getMaxSearches()){
            return true;
        }
        else return false;
    }

    public function getMaxSearches()
    {
        return max(0, (int) Mage::getStoreConfig('aligent_storelocator/settings/max_searches'));
    }

    /**
     * Retrieve number of searches by current IP address
     *
     * @return int
     */
    private function _searchesByUser()
    {
        $this->_deleteLogsBefore(time() - $this->_period);

        $amount = $this->getSearchCount($this->_ip, time() - $this->_period);

        $this->setData(array('ip'=>$this->_ip, 'time'=>time()));
        $this->save();

        return $amount;
    }

    /**
     * Delete Before Log
     *
     * @param int $time
     * @return Mage_Sendfriend_Model_Sendfriend
     */
    private function _deleteLogsBefore($time)
    {
        $this->_getResource()->deleteLogsBefore($time);
        return $this;
    }

    public function getSearchCount($ip, $startTime)
    {
        $count = $this->_getResource()->getSearchCount($this, $ip, $startTime);
        return $count;
    }

    public function setIp($ip)
    {
        $this->_ip = $ip;
    }

}
