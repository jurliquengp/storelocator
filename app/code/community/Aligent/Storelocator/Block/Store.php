<?php
/**
 * Store.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Store
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Store extends Mage_Core_Block_Template
{

    /** @var  Aligent_Storelocator_Model_Storelocation */
    protected $store = null;

    public function getStore()
    {
        $store = $this->store;
        if (is_null($store) || is_numeric($store)) {
            if (is_null($store)) {
                $store = Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY);
            }
            if (is_numeric($store)) {
                $store = Mage::getModel('aligent_storelocator/storelocation')->load($store);
            }
            if (!$store instanceof Aligent_Storelocator_Model_Storelocation || $store->isObjectNew()) {
                throw new Exception('Invalid Store');
            }
            $this->store = $store;
        }

        return $store;
    }

    /**
     * @param int|Aligent_Storelocator_Model_Storelocation $store A store object, or store Id.
     *
     * @throws Exception
     */
    public function setStore($store)
    {
        if (!is_numeric($store)
            && (!$store instanceof Aligent_Storelocator_Model_Storelocation || $store->isObjectNew())
        ) {
            throw new Exception('Invalid Store');
        }
        $this->store = $store;
    }
}
