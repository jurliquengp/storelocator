<?php
/**
 * Storelocator.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Storelocator
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Storelocator extends Mage_Core_Block_Template
{

    protected function _prepareLayout()
    {
        $this->getLayout()->getBlock('head')->addItem('skin_js', 'aligent/js/StoreLocator.js');

        return parent::_prepareLayout();
    }
}
