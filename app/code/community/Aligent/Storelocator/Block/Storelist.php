<?php
/**
 * Storelist.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Storelist
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Storelist extends Mage_Core_Block_Template
{

}
