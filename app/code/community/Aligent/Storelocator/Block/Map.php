<?php
/**
 * Map.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Map
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Map extends Mage_Core_Block_Template
{

    protected function _prepareLayout()
    {
        $sMapApiUrl = Mage::getStoreConfig('aligent_storelocator/google_api/maps_api_url');
        $sMapApiKey = Mage::getStoreConfig('aligent_storelocator/google_api/maps_api_key');

        $oLayout = $this->getLayout();
        $oBlock  = $oLayout->createBlock('core/text')->setText(
                           '<script src="' . $sMapApiUrl . '?key=' . $sMapApiKey . '&amp;sensor=false"></script>'
        );
        $this->getLayout()->getBlock('after_body_start')->append($oBlock);

        return parent::_prepareLayout();
    }
}
