<?php
/**
 * Form.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Adminhtml_Storelocation_Edit_Form
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Adminhtml_Storelocation_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                                         'id'      => 'edit_form',
                                         'action'  => $this->getData('action'),
                                         'method'  => 'post',
                                         'enctype' => 'multipart/form-data'
                                     ));

        $store = Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY);

        if ($store->getId()) {
            $form->addField(
                 'entity_id',
                 'hidden',
                 array(
                     'name' => 'storelocation_id',
                 )
            );
            $form->setValues($store->getData());
        }

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
