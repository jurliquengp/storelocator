<?php
/**
 * Grid.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Adminhtml_Storelocation_Grid
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Adminhtml_Storelocation_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('storelocatorStorelocationGrid');
        $this->setDefaultSort('name');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('aligent_storelocator/storelocation')->getCollection()
                          ->addAttributeToSelect('*');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
             'name',
             array(
                 'header' => Mage::helper('aligent_storelocator')->__('Name'),
                 'align'  => 'left',
                 'index'  => 'name',
             )
        );

        $this->addColumn(
             'city',
             array(
                 'header' => Mage::helper('aligent_storelocator')->__('City'),
                 'align'  => 'left',
                 'index'  => 'city',
             )
        );

        $this->addColumn(
             'state',
             array(
                 'header' => Mage::helper('aligent_storelocator')->__('State'),
                 'align'  => 'left',
                 'index'  => 'state',
             )
        );

        $this->addColumn(
             'postcode',
             array(
                 'header' => Mage::helper('aligent_storelocator')->__('Postcode'),
                 'align'  => 'left',
                 'index'  => 'postcode',
             )
        );

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        return parent::_prepareColumns();
    }


    /**
     * Iterate collection and call callback method per item
     * For callback method first argument always is item object
     *
     * @param string $callback
     * @param array $args additional arguments for callback method
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    public function _exportIterateCollection($callback, array $args)
    {
        /** @var $originalCollection Aligent_Storelocator_Model_Resource_Storelocation_Collection */

        $originalCollection = $this->getCollection();
        $count = null;
        $page  = 1;
        $lPage = null;
        $break = false;

        while ($break !== true) {
            $collection = clone $originalCollection;
            /**
             * To work around a bug in magento core
             * Paging kicks in when record is more than page limit (usually 1000 records)
             * collection clone is shallow, so select object is shared between clones
             * when collections orders are rendered, magento adds join for sort attribute
             * @see Mage_Eav_Model_Entity_Collection_Abstract::addAttributeToSort()
             *
             * During second page execution sorting > join is called again for same select object
             * Join fails because join with same $_correlationName already exists
             * @see Zend_Db_Select::_join()
             *
             * So cloning select too
             */
            $collection->setSelect(clone $originalCollection->getSelect());
            $collection->setPageSize($this->_exportPageSize);
            $collection->setCurPage($page);
            $collection->load();
            if (is_null($count)) {
                $count = $collection->getSize();
                $lPage = $collection->getLastPageNumber();
            }
            if ($lPage == $page) {
                $break = true;
            }
            $page ++;

            foreach ($collection as $item) {
                call_user_func_array(array($this, $callback), array_merge(array($item), $args));
            }
        }
    }

    /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
