<?php
/**
 * Storeinformation.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Adminhtml_Storelocation_Edit_Tab_Storeinformation
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Adminhtml_Storelocation_Edit_Tab_Storeinformation
    extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Initialize form
     *
     * @return $this
     */
    public function initForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('_store_information_');
        $form->setFieldNameSuffix('store_information');

        $storeLocation = Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY);

        /** @var $storeForm Aligent_Storelocator_Model_Form */
        $storeForm = Mage::getModel('aligent_storelocator/form');
        $storeForm->setEntity($storeLocation)
                  ->setFormCode('adminhtml_storelocation')
                  ->initDefaultValues();

        $fieldset = $form->addFieldset(
                         'base_fieldset',
                         array(
                             'legend' => Mage::helper('aligent_storelocator')->__('Store Information')
                         )
        );

        $attributes = $storeForm->getAttributes();
        foreach ($attributes as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
            $attribute->setFrontendLabel(Mage::helper('aligent_storelocator')->__($attribute->getFrontend()->getLabel()));
            $attribute->unsIsVisible();
        }

        $this->_setFieldset($attributes, $fieldset, array());

//		if ($store->isReadonly()) {
//			foreach ($store->getAttributes() as $attribute) {
//				$element = $form->getElement($attribute->getAttributeCode());
//				if ($element) {
//					$element->setReadonly(true, true);
//				}
//			}
//		}
        $form->setValues($storeLocation->getData());
        $this->setForm($form);

        return $this;
    }

    protected function _getAdditionalElementTypes()
    {
        return array(
            'image' => Mage::getConfig()->getBlockClassName('aligent_storelocator/adminhtml_storelocation_helper_image'),
            'textarea' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_helper_form_wysiwyg'),
            'opening_hours' => Mage::getConfig()->getBlockClassName('aligent_storelocator/adminhtml_storelocation_helper_openinghours')
        );
    }

}
