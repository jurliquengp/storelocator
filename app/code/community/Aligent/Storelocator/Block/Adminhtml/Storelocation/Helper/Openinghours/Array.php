<?php

class Aligent_Storelocator_Block_Adminhtml_Storelocation_Helper_Openinghours_Array
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('storelocation/openinghours/array.phtml');

        $this->addColumn('day', array(
            'label' => Mage::helper('aligent_storelocator')->__('Day'),
            'style' => 'width:80px',
        ));
        $this->addColumn('open_time', array(
            'label' => Mage::helper('aligent_storelocator')->__('Open Time (e.g. 9am - 5pm)'),
            'style' => 'width:200px',
        ));
    }
}
