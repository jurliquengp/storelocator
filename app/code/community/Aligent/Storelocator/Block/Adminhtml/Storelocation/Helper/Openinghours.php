<?php

/**
 * Aligent_Storelocator_Block_Adminhtml_Storelocation_Helper_Openinghours
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Matthew O'Loughlin <matthew.oloughlin@aligent.com.au>
 * @copyright 2016 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Adminhtml_Storelocation_Helper_Openinghours extends Varien_Data_Form_Element_Abstract
{

    protected $_arrayBlock;

    public function __construct()
    {
        parent::__construct();
        $this->_arrayBlock = Mage::app()->getLayout()->createBlock('aligent_storelocator/adminhtml_storelocation_helper_openinghours_array');
    }

    public function getHtml() {
        $this->_arrayBlock->setElement($this);
        $html = $this->_arrayBlock->toHtml();
        $this->_arrayBlock->setArrayRowsCache(null);

        return $html;
    }

    public function getName()
    {
        return parent::getName() . "[" . $this->getEntityAttribute()->getAttributeCode() . "]";
    }
}
