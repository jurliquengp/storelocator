<?php
/**
 * Edit.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Adminhtml_Storelocation_Edit
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Adminhtml_Storelocation_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_controller = 'adminhtml_storelocation';
        $this->_blockGroup = 'aligent_storelocator';

        parent::__construct();

        $this->setData('form_action_url', $this->getUrl('*/*/save'));

        $this->_updateButton('save', 'label', Mage::helper('aligent_storelocator')->__('Save Location'));
        $this->_updateButton('delete', 'label', Mage::helper('aligent_storelocator')->__('Delete Location'));

        $this->_addButton(
             'geolocate',
             array(
                 'label'   => Mage::helper('adminhtml')->__('Get Lat/Long'),
                 'onclick' => 'geolocateAddress()',
             ),
             -1
        );

//		if (Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY)->isReadonly()) {
//			$this->_removeButton('save');
//			$this->_removeButton('reset');
//		}
//
//		if (!Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY)->isDeleteable()) {
//			$this->_removeButton('delete');
//		}
    }

    public function getHeaderText()
    {
        if (Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY)->getId()) {
            return $this->escapeHtml(
                        Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY)->getName()
            );
        } else {
            return Mage::helper('aligent_storelocator')->__('New Store Location');
        }
    }

    /**
     * Prepare form html. Add block for configurable product modification interface
     *
     * @return string
     */
    public function getFormHtml()
    {
        $html = parent::getFormHtml();
        $html .= $this->getLayout()->createBlock('adminhtml/catalog_product_composite_configure')->toHtml();

        return $html;
    }

    public function getValidationUrl()
    {
        return $this->getUrl('*/*/validate', array('_current' => true));
    }

    protected function _prepareLayout()
    {
//		if (!Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY)->isReadonly()) {
        $this->_addButton(
             'save_and_continue',
             array(
                 'label'   => Mage::helper('aligent_storelocator')->__('Save and Continue Edit'),
                 'onclick' => 'saveAndContinueEdit(\'' . $this->_getSaveAndContinueUrl() . '\')',
                 'class'   => 'save'
             ),
             10
        );

//		}

        return parent::_prepareLayout();
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl(
                    '*/*/save',
                    array(
                        '_current' => true,
                        'back'     => 'edit',
                        'tab'      => '{{tab_id}}'
                    )
        );
    }
}
