<?php
/**
 * Tabs.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Adminhtml_Storelocation_Edit_Tabs
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Adminhtml_Storelocation_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('storelocation_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('aligent_storelocator')->__('Store Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab(
             'store_information',
             array(
                 'label'   => Mage::helper('aligent_storelocator')->__('Store Information'),
                 'content' => $this->getLayout()
                                   ->createBlock('aligent_storelocator/adminhtml_storelocation_edit_tab_storeinformation')
                                   ->initForm()
                                   ->toHtml(),
             )
        );

//        if (Mage::registry(Aligent_Storelocator_Helper_Data::CURRENT_STORELOCATION_REGISTRY_KEY)->getId()) {
//            // Example of Ajax loaded tab.
//			$this->addTab('tab2', array(
//				'label'     => Mage::helper('aligent_storelocator')->__('Another Tab'),
//				'class'     => 'ajax',
//				'url'       => $this->getUrl('*/*/tab2', array('_current' => true)),
//			));
//        }

        //$this->_updateActiveTab();
        return parent::_beforeToHtml();
    }

    protected function _updateActiveTab()
    {
        $tabId = $this->getRequest()->getParam('tab');
        if ($tabId) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if ($tabId) {
                $this->setActiveTab($tabId);
            }
        }
    }
}
