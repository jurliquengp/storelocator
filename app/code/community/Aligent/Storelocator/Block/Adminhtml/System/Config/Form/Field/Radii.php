<?php

class Aligent_Storelocator_Block_Adminhtml_System_Config_Form_Field_Radii extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    public function __construct()
    {
        $this->addColumn('radii', array(
            'label' => Mage::helper('aligent_storelocator')->__('Radii'),
            'style' => 'width:250px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('aligent_storelocator')->__('Add Radius');
        parent::__construct();
    }
}