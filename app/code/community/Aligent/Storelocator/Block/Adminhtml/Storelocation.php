<?php
/**
 * Storelocation.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Adminhtml_Storelocation
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Jim O'Halloran <jim@aligent.com.au>
 * @author    Luke Mills <luke@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Adminhtml_Storelocation extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup     = 'aligent_storelocator';
        $this->_controller     = 'adminhtml_storelocation';
        $this->_headerText     = Mage::helper('aligent_storelocator')->__('Manage Store Locations');
        $this->_addButtonLabel = Mage::helper('aligent_storelocator')->__('Add New Location');
        parent::__construct();
    }
}
