<?php
/**
 * Map.php
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

/**
 * Aligent_Storelocator_Block_Search
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */
class Aligent_Storelocator_Block_Search extends Mage_Core_Block_Template
{

    public function getRadii()
    {
        $sRadii = Mage::getStoreConfig('aligent_storelocator/settings/radii');
        if (!$sRadii) {
            return $aRadii = array(10, 20, 50, 100, 500);
        }

        $aRadii = unserialize($sRadii);

        $aResult = array();
        foreach ($aRadii as $aRadius) {
            $aResult[] = (int) $aRadius['radii'];
        }

        return $aResult;
    }

}
