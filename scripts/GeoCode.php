<?php

class Geo {

    const LONG = 'long';
    const NORMAL = 'normal';
    const SHORT = 'short';

    public $geocodeApi;

    public function getGeocodeFromAddress($sAddress, $bSensor = false, $vRegionBias = false) {
        $sRequest = $this->geocodeApi . '?address=' . urlencode($sAddress) . '&sensor=' . $bSensor;
        if ($vRegionBias !== false) {
            $sRequest .= '&region=' . $vRegionBias;
        }

        $oResponse = json_decode(file_get_contents($sRequest));
        if ($oResponse->results[0]) {
            return $oResponse->results[0];
        } else {
            return false;
        }
    }

    public function getGeolocationFromAddress($sAddress, $bSensor = false, $vRegionBias = false) {
        $oResult = $this->getGeocodeFromAddress($sAddress, $bSensor, $vRegionBias);
        if ($oResult) {
            return $oResult->geometry->location;
        }
    }

    public function getFormattedAddressFromAddress($sAddress, $sFormat = self::NORMAL, $bSensor = false, $vRegionBias = false) {
        $oResult = $this->getGeocodeFromAddress($sAddress, $bSensor, $vRegionBias);
        if (!$oResult) return false;

        switch ($sFormat) {
            case self::NORMAL :
                return $oResult->formatted_address;
                break;
            case self::LONG :
                $sResponse = '';
                foreach ($oResult->address_components as $oComponent) {
                    $sResponse .= $oComponent->long_name . ' ';
                }
                return trim($sResponse);
                break;
            case self::SHORT :
                $sResponse = '';
                foreach ($oResult->address_components as $oComponent) {
                    $sResponse .= $oComponent->short_name . ' ';
                }
                return trim($sResponse);
                break;
        }
    }

}

$oGeo = new Geo();
$oGeo->geocodeApi = 'https://maps.googleapis.com/maps/api/geocode/json';
$oGeocode = $oGeo->getGeolocationFromAddress($_GET['address'], false, isset($_GET['region']) ?: false);

header('Content-Type: application/json');
echo json_encode($oGeocode);
