/**
 * StoreLocator.js
 *
 * @category  Aligent
 * @package   Aligent_Storelocator
 * @author    Brent Honeybone <brent@aligent.com.au>
 * @copyright 2014 Aligent Consulting.
 * @license   All Rights Reserved
 * @link      http://www.aligent.com.au/
 */

if (typeof Aligent == 'undefined') var Aligent = {};

/* /////////////////////////////
 //// Nl2br Helper for Handlebars
 /////////////////////////////// */

Handlebars.registerHelper('nl2br', function(text) {
    if (!text || typeof text == 'undefined') return;
    var nl2br = (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
    return new Handlebars.SafeString(nl2br);
});

Handlebars.registerHelper('ifHas', function(property, value, options) {
    if(property.indexOf(value) !== -1) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('ifLast', function(ignoreFirst, options) {
    if (options.data.first && ignoreFirst) {
        return;
    }
    if (options.data.last) {
        return options.fn(this);
    }
    return options.inverse(this);
});

/* /////////////////////////////
 //// Event Manager
 /////////////////////////////// */

Aligent.EventManager = Class.create({

    listen: function(event, callback, el) {
        if (this._events[event]) {
            this._events[event].push({ callback: callback, el: el });
        } else {
            this._events[event] = [{ callback: callback, el: el }];
        }
        return this;
    },

    stopListening: function(event, callback) {
        if (!this._events[event]) return false;
        if (typeof callback == 'undefined') {
            this._events[event] = null;
        } else {
            var i = 0, index = -1;
            for (var item in this._events[event]) {
                if (this._events[event].callback == callback) {
                    index = i;
                }
                i ++;
            }

            if (index != -1) {
                this._events[event].splice(index, 1);
            }
        }
        return this;
    },

    dispatch: function(event) {
        var i = 0, that = this;

        if (!this._events[event]) return false;

        for (i; i < this._events[event].length; i++) {
            this._events[event][i].callback.apply(that._events[event][i].el, [ that ]);
        }

        return this;
    }
});

/* /////////////////////////////
 //// Ajax
 /////////////////////////////// */

Aligent.Ajax = Class.create(Aligent.EventManager, {
    initialize: function(url, params, method, attempts) {
        this.url = url;
        this.params = params || {};
        this.method = method || Aligent.Ajax.GET;
        this.response = null;
        this.attempts = attempts || 0;
        this.currentAttempt = 0;
        this._events = {};

        if (this.params && this.method == 'get') {
            var that = this, count = 0;
            that.url += '?';
            for (var i in that.params) {
                if (count !== 0) {
                    that.url += '&'
                }
                that.url += (i + '=' + encodeURIComponent(that.params[i]));
                count++;
            }
        }

        return this;
    },

    send: function() {
        var config = {
            method: this.method,
            onSuccess: this.onSuccess.bind(this),
            onFailure: this.onFailure.bind(this)
        };

        if (this.method == 'post' && this.params) {
            config.parameters = this.params;
        }

        new Ajax.Request(this.url, config);
        return this;
    },

    onSuccess: function(response) {
        if (response.status != 200) {
            this.onFailure(response);
            return;
        }

        this.currentAttempt++;
        this.response = response;
        this.dispatch(Aligent.Ajax.AJAX_SUCCESS);
    },

    onFailure: function(response) {
        this.currentAttempt++;
        this.response = response;
        this.dispatch(Aligent.Ajax.AJAX_FAILURE);
        if (this.currentAttempt >= this.attempts) {
            this.dispatch(Aligent.Ajax.AJAX_FAILED);
        } else {
            this.send();
        }
    },

    getResponseStatus: function() {
        return this.response.status;
    },

    getResponse: function(as) {
        if (typeof as == 'undefined') as = Aligent.Ajax.JSON;

        switch (as) {
            case Aligent.Ajax.JSON :
                return this.response.responseJSON;
                break;
            case Aligent.Ajax.TEXT :
                return this.response.responseText;
                break;
        }
    }
});

Aligent.Ajax.AJAX_SUCCESS = 'ajaxSuccess';
Aligent.Ajax.AJAX_FAILURE = 'ajaxFailure';
Aligent.Ajax.AJAX_FAILED = 'ajaxFailed';

Aligent.Ajax.GET = 'get';
Aligent.Ajax.POST = 'post';

Aligent.Ajax.JSON = 'json';
Aligent.Ajax.TEXT = 'text';

/* /////////////////////////////
 //// Map Data
 /////////////////////////////// */

Aligent.MapData = Class.create(Aligent.EventManager, {
    initialize: function(data) {
        this._events = {};
        this._data = data || { total: 0, results: [] };

        if (this._data) {
            this.dataUpdated();
        }
    },

    dataUpdated: function() {
        this.dispatch(Aligent.MapData.DATA_CHANGE);
    },

    getLocations: function() {
        return this._data.results;
    },

    getLocation: function(id) {
        var result = false;
        this._data.results.each(function(item) {
            if (item.id == id) {
                result = item;
                throw $break;
            }
        });
        return result;
    },

    getAmount: function() {
        return this._data.total;
    },

    getData: function() {
        return this._data;
    },

    setData: function(data) {
        this._data = data;
        this.dataUpdated();
    }

});

Aligent.MapData.DATA_CHANGE = 'dataChange';

/* /////////////////////////////
 //// Map
 /////////////////////////////// */

Aligent.Map = Class.create(Aligent.EventManager, {
    initialize: function(containerId, options) {
        this._options = typeof(options) === 'undefined' ? {} : options;
        this._events = {};
        this.containerId = containerId;
        this._map = null;

        google.maps.event.addDomListener(window, 'load', this.setupMap.bind(this));
    },

    setDefaultOptions: function() {
        this.setDefaultOption('zoom', 2);
        this.setDefaultOption('mapTypeId', google.maps.MapTypeId.ROADMAP);
        this.setDefaultOption('center', new google.maps.LatLng(0, 0));
    },

    setDefaultOption: function(key, value) {
        if (typeof(this._options[key]) === 'undefined') this._options[key] = value;
    },

    setupMap: function() {
        this.setDefaultOptions();
        this._map = new google.maps.Map(document.getElementById(this.containerId), this._options);
        this.dispatch(Aligent.Map.MAP_INIT);
    },

    getMap: function() {
        return this._map;
    },

    fitBounds: function(bounds) {
        this._map.fitBounds(bounds);
    },

    setCenter: function(coords, zoom) {
        this._map.panTo(coords);
        this._map.setZoom(zoom);
    }

});

Aligent.Map.MAP_INIT = 'mapInit';

/* /////////////////////////////
 //// Map Location Manager
 /////////////////////////////// */

Aligent.MapLocationManager = Class.create(Aligent.EventManager, {
    initialize: function(map, dataManager, autoRecenter) {
        this._events = {};
        this._recenter = autoRecenter;
        if(typeof autoRecenter === "undefined") {
            this._recenter = true;
        }
        this._map = map;
        this._dataManager = dataManager;

        this.locations = {};

        this._mapReady = false;
        this._dataReady = false;

        this._ready = false;

        this._bindEvents();
        if (this._dataManager.getAmount()) {
            this._dataReady = true;
        }
    },

    _bindEvents: function() {
        this._map.listen(Aligent.Map.MAP_INIT, this._mapIsReady.bind(this));
        this._dataManager.listen(Aligent.MapData.DATA_CHANGE, this._dataIsReady.bind(this));
    },

    _mapIsReady: function() {
        this._mapReady = true;
        this._readyTest();
    },

    _dataIsReady: function() {
        this.removeLocations();

        if (this._ready && this._dataManager.getData().results.length) {
            this.refreshLocations();
            return;
        }
        this._dataReady = true;
        this._readyTest();
    },

    _readyTest: function() {
        if (this._dataReady && this._mapReady) {
            this._ready = true;
            if (this._dataManager.getData().results.length) {
                this.refreshLocations(true);
            }
        }
    },
    removeLocations: function() {
        for (var i in this.locations) {
            this.locations[i].removeFromMap();
            if (this.locations[i].directions) {
                this.locations[i].directions.removeRoute();
            }
        }
    },

    refreshLocations: function(recenter) {
        var locations = this._dataManager.getLocations(), that = this, i;
        if (typeof recenter === 'undefined') recenter = this._recenter;

        that.removeLocations();

        that.locations = {};

        locations.each(function(item) {
            var locationConfig = {},
                location,
                locationInfo,
                i;

            for (i in item) {
                if (i != 'location') {
                    locationConfig[i] = item[i];
                }
            }

            locationConfig.mapInstance = that._map;

            location = new Aligent.MapLocation(item.location.la, item.location.lo, locationConfig);
            that.locations[item.id] = location;
            item.location = location;

            locationInfo = new Aligent.MapLocationInfo(item);
            locationInfo.listen(Aligent.MapLocationInfo.MARKER_CLICK, that.removeInfoWindows.bind(that));

            location.addToMap(that._map.getMap());
        });

        if (recenter) {
            if (locations.length <= 1) {
                locations[locations.length-1].location.centerOnMap();
            } else {
                this._map.fitBounds(this.getLocationBounds(this.locations));
            }
        }
    },

    removeInfoWindows: function() {
        this._dataManager.getLocations().each(function(location) {
            location.location.infoWindow.close();
        });
    },

    getLocationBounds: function(locations) {
        var bounds = new google.maps.LatLngBounds();
        for (var i in locations) {
            bounds.extend(locations[i].getCoordinates());
        }

        return bounds;
    }
});

/* /////////////////////////////
 //// Map Location
 /////////////////////////////// */

Aligent.MapLocation = Class.create(Aligent.EventManager, {
    initialize: function(lat, lon, config) {
        this._events = {};
        this.defaultZoom = 18;

        if (typeof config.pin !== 'undefined') {
            var image = {
                url: config.pin
            };

            config.icon = image;
        }

        if (typeof config == 'undefined') {
            this._config = {};
        } else {
            this._config = config;
        }

        this._config.position = new google.maps.LatLng(lat, lon);

        if (typeof this._config.zoom == 'undefined') {
            this._config.zoom = this.defaultZoom;
        }

        this._marker = new google.maps.Marker(this._config);
    },

    addToMap: function(map) {
        this._marker.setMap(map);
    },

    removeFromMap: function() {
        this._marker.setMap(null);
    },

    hide: function() {
        this._marker.setVisible(false);
    },

    show: function() {
        this._marker.setVisible(true);
    },

    getMarker: function() {
        return this._marker;
    },

    getCoordinates: function() {
        return this._marker.getPosition()
    },

    centerOnMap: function(zoom) {
        this._config.mapInstance.setCenter(this.getCoordinates(), zoom || this._config.zoom);
    }

});

Aligent.MapLocationInfo = Class.create(Aligent.EventManager, {
    initialize: function(location, template) {
        this.marker = location.location.getMarker();
        this.template = template;
        this.location = location;
        this.location.location.infoWindow;
        this._events = {};

        this.create();
        this.bindEvents();
    },

    create: function() {
        this.location.location.infoWindow = new google.maps.InfoWindow({
            content: Aligent.MapLocationInfo.TEMPLATE(this.location)
        });
    },

    bindEvents: function() {
        var that = this;
        google.maps.event.addListener(this.marker, 'click', function() {
            that.dispatch(Aligent.MapLocationInfo.MARKER_CLICK);
            that.location.location.infoWindow.open(that.marker.map, that.marker);
            var markerJson =  Aligent.AnalyticsEvent.stringify(that.marker);
            Aligent.AnalyticsEvent.trackEvent('location-click', markerJson);
        });
    }

});

Aligent.MapLocationInfo.TEMPLATE = Handlebars.compile('<div class="store-info-window"><h3 id="location-window-{{id}}" class="info-window">{{name}}</h3><div class="details">{{description}}</div><div class="directions" data-id="{{id}}">Get Directions</div></div>');
Aligent.MapLocationInfo.MARKER_CLICK = 'markerClick';

/* /////////////////////////////
 //// Map Direction Manager
 /////////////////////////////// */

Aligent.MapDirectionManager = Class.create(Aligent.EventManager, {
    initialize: function(elClass, dataIdName, data, map) {
        this.elClass = elClass;
        this.dataIdName = dataIdName;
        this._events = {};
        this.bindEvents();
        this.data = data;
        this.map = map;
    },

    bindEvents: function() {
        document.observe('click', this.directionsClick.bind(this));
    },

    directionsClick: function(ev) {
        var el = Event.element(ev),
            that = this;
        if (el.hasClassName(this.elClass) && el.hasAttribute(this.dataIdName)) {
            var location = Aligent.mapData.getLocation(el.readAttribute('data-id')).location;

            this.data.getLocations().each(function(loc) {
                if (typeof loc.location.directions != 'undefined' && loc.location.directions && loc.location.directions.removeRoute != 'undefined') {
                    loc.location.directions.removeRoute();
                }
            });

            location.directions = new Aligent.MapDirections([ this.data.getLocation(0).location, this.data.getLocation(el.readAttribute('data-id')).location ]);
            location.directions.getRoute();
            location.directions.listen(Aligent.MapDirections.GOT_ROUTES, function() {
                location.directions.renderRoute(that.map.getMap());
            });

            this.dispatch(Aligent.MapDirectionManager.DIRECTIONS_CLICK);
        }
    }
});

Aligent.MapDirectionManager.DIRECTIONS_CLICK = 'directionsClick';

/* /////////////////////////////
 //// Map Location Directions
 /////////////////////////////// */

Aligent.MapDirections = Class.create(Aligent.EventManager, {
    initialize: function(locations) {
        this._events = {};
        this.locations = locations;
        this.service = new google.maps.DirectionsService();
        this._path = null;
        this._poly = null;
        this.defaultRouteColor = '#ff0099';
        this._isRendered = false;
        this.routes = false;
    },

    getRoute: function() {
        var that = this,
            config = {
                origin: this.locations[0].getCoordinates(),
                destination: this.locations[1].getCoordinates(),
                travelMode: google.maps.DirectionsTravelMode.DRIVING // configurable?
                // unitSystem: google.maps.UnitSystem.IMPERIAL // for miles
            };

        this.service.route(config, function(result, status) {
            if (status == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
                setTimeout(function() {
                    that.getRoute();
                }, 1000)
            } else {
                that._gotRoute(status, result);
            }
        });
    },

    _gotRoute: function(status, result) {
        if (status != google.maps.DirectionsStatus.OK) return;
        this.routes = result.routes;
        this.dispatch(Aligent.MapDirections.GOT_ROUTES);
    },

    renderRoute: function(map) {
        if (this._isRendered) return;
        this._poly = new google.maps.Polyline({ path: this.routes[0].overview_path, strokeColor: this.locations[this.locations.length - 1 ].getMarker().color });
        this._poly.setMap(map);
        map.fitBounds(this.routes[0].bounds);
        this._isRendered = true;
    },

    removeRoute: function() {
        if (this._poly) {
            this._poly.setMap(null);
        }
    },

    getInstructions: function() {
        var instructions = [];
        this.routes[0].legs[0].steps.each(function(step) {
            instructions.push({ instruction: step.instructions, distance: step.distance, duration: step.duration });
        });
        return instructions;
    },

    getDistance: function() {
        this.routes[0].legs[0].distance;
    },

    getDuration: function() {
        this.routes[0].legs[0].duration;
    }


});

Aligent.MapDirections.GOT_ROUTES = 'gotRoutes';

/* /////////////////////////////
 //// Map Location List
 /////////////////////////////// */

Aligent.StoreList = Class.create(Aligent.EventManager, {
    initialize: function(dataManager, config) {
        this._events = {};
        this._config = config;
        this._template = Handlebars.compile($$(config.template).first().innerHTML);
        this.container = $$(config.containers);
        this._dataManager = dataManager;
        this._dataManager.listen(Aligent.MapData.DATA_CHANGE, this._dataChange.bind(this));
        this.filters = [];

        if (typeof config.scrollToMap === 'undefined') {
            this._config.scrollToMap = 'map';
        }
        if (typeof config.fixHeight === 'undefined') {
            this._config.fixHeight = true;
        }
        if (config.updateOnInitialize &&  this._dataManager.getLocations().length) {
            this.update();
        }
    },

    _dataChange: function() {
        this.update();
    },

    _structuredClone: function(obj) {
        var clone = JSON.stringify(obj);
        return JSON.parse(clone);
    },

    addFilter: function(filter) {
        var that = this;
        filter.listen('filtered', function() {
            that.container.each(function(container) {
                Aligent.classify.down(container.childElements());
                Aligent.classify.up(container.childElements());
            });
        });
        this.filters.push(filter);
    },

    update: function() {
        var that = this;
        that.container.each(function(container) {
            container.update('');
        });
        this._dataManager.getLocations().each(function(item) {

            if (item.showInList === false) {
                return;
            }

            var data = that._structuredClone(item),
                el, holder;

            if (typeof data.distance != 'undefined' && typeof that._config.distanceRound != 'undefined') {
                var by = 0;
                if (that._config.distanceRound < 0) {
                    by = Math.pow(10, Math.abs(that._config.distanceRound));
                    data.distance = Math.round(data.distance * by) / by;
                } else if (that._config.distanceRound > 0) {
                    by = Math.pow(10, Math.abs(that._config.distanceRound));
                    data.distance = Math.round(data.distance / by) * by;
                } else {
                    data.distance = Math.round(data.distance);
                }
            }

            el = that._template(data);

            item.listItems = [];

            holder = new Element('div');

            that.container.each(function(container) {
                var newEl;
                holder.update(el);
                newEl = holder.childElements().first();
                container.insert({ bottom: newEl });
                if (typeof item.listItems == 'undefined') {
                    item.listItems = [];
                }
                item.listItems.push(newEl);
            });
        });

        $$(that._config.containers).each(function(item) {
            item.select(that._config.showOnMapSelector).invoke('observe', 'click', that._showOnMapClick.bind(that));
            Aligent.classify.down(item.childElements());
            Aligent.classify.up(item.childElements());
        });

        if (that._config.fixHeight) {
            that.container.each(function (item) {
                that.fixHeight(item);
            });
        }
    },

    fixHeight: function(container) {
        var maxHeight = 0, height;

        container.childElements().each(function(item) {
            height = item.getLayout().get('border-box-height');
            if (height > maxHeight) {
                maxHeight = height;
            }
        });

        container.childElements().each(function(item) {
            item.setStyle({ "min-height": maxHeight + 'px' });
        });
    },

    _showOnMapClick: function(ev) {
        var el = Event.element(ev);
        if (typeof this._config.showOnMapParentId !== 'undefined') {
            var par = el.up(this._config.showOnMapParentId);
            if (typeof par !== 'undefined') {
                el = par;
            }
        }

        var id = el.readAttribute('data-id'),
            data = this._dataManager.getLocation(id);

        if (typeof data != 'undefined' && typeof data.location !== 'undefined') {
            data.location.centerOnMap(data.zoom);
            this._dataManager.getLocations().each(function(location) {
                location.location.infoWindow.close();
            });
            data.location.infoWindow.open(data.location.getMarker().map, data.location.getMarker());
        }

        if (this._config.scrollToMap) {
            Effect.ScrollTo(this._config.scrollToMap);
        }
        var dataJson =  Aligent.AnalyticsEvent.stringify(data);
        Aligent.AnalyticsEvent.trackEvent('show-on-map-click', dataJson);
    }

});

/* /////////////////////////////
 //// Classify
 /////////////////////////////// */

Aligent.Classify = Class.create({
    initialize: function(classifications) {
        if (typeof classifications == 'undefined') {
            this.classifications = [];
        } else {
            this.classifications = classifications;
        }
    },

    addClassification: function(classification) {
        this.classifications.push(classification);
    },

    up: function(items) {
        var count = 0, that = this;
        items.each(function(item) {
            if (!item.visible()) {
                return;
            }
            that.classifications.each(function(classification) {
                classification.up(item, count);
            });
            count++;
        });
    },

    down: function(items) {
        var count = 0, that = this;
        items.each(function(item) {
            that.classifications.each(function(classification) {
                classification.down(item, count);
            });
            count++;
        });
    }
});

/* Count */

Aligent.ClassificationCount = Class.create({

    initialize: function(prefix) {
        if (typeof prefix !== 'undefined') {
            this.prefix = prefix;
        } else {
            this.prefix = 'item-';
        }
    },

    up: function(item, count) {
        item.addClassName(this.prefix + (count + 1));
    },

    down: function(item, count) {
        item.removeClassName(this.prefix + (count + 1));
    }

});

/* Zebra */

Aligent.ClassificationZebra = Class.create({

    initialize: function(prefix, name) {
        if (typeof prefix !== 'undefined' && prefix) {
            this.prefix = prefix;
        } else {
            this.prefix = '';
        }

        if (typeof name !== 'undefined' && name) {
            this.name = name;
        } else {
            this.name = 'zebra';
        }
    },

    up: function(item, count) {
        if ((count + 1) % 2 === 0) {
            item.addClassName(this.prefix + this.name);
        }
    },

    down: function(item, count) {
        if ((count + 1) % 2 === 0) {
            item.removeClassName(this.prefix + this.name);
        }
    }

});

/* Nth */

Aligent.ClassificationNths = Class.create({

    initialize: function(prefix, nths) {
        if (typeof prefix !== 'undefined' && prefix) {
            this.prefix = prefix;
        } else {
            this.prefix = 'nth-';
        }

        if (typeof nths !== 'undefined' && nths) {
            this.nths = nths;
        } else {
            this.nths = [ 3, 5 ];
        }
    },

    up: function(item, count) {
        var that = this;
        this.nths.each(function(nth) {
            if ((count + 1) % nth === 0) {
                item.addClassName(that.prefix + nth);
            }
        });
    },

    down: function(item, count) {
        var that = this;
        this.nths.each(function(nth) {
            item.removeClassName(that.prefix + nth);
        });
    }

});

Aligent.classifications = [ new Aligent.ClassificationNths(null, [ 2 ]) ];

Aligent.classify = new Aligent.Classify(Aligent.classifications);

/* /////////////////////////////
 //// Filter
 /////////////////////////////// */

Aligent.StoreFilter = Class.create(Aligent.EventManager, {
    initialize: function(container, attribute, data, showAllOption, showAllLabel) {
        this._events = {};
        this.data = data;
        this.attribute = attribute;
        this.showAllOption = showAllOption;
        this.showAllLabel = showAllLabel;
        this.container = container;
        this.bindEvents();
        this.update();
    },

    bindEvents: function() {
        this.data.listen(Aligent.MapData.DATA_CHANGE, this.update.bind(this));
    },

    getCurrentValue: function() {
        var option = this.container.select('option');
        if (option.length) {
            option = option[this.container.selectedIndex];
            return option.readAttribute('data-value');
        }
    },

    update: function() {
        var options = this.getOptions(),
            el,
            that = this,
            type = 'div',
            tagName = this.container.tagName.toLowerCase();
        if (tagName == 'ul') {
            type = 'li';
        } else if (tagName == 'select') {
            type = 'option';
            this.container.observe('change', function() {
                that.filter(that.getCurrentValue());
            });
        }

        that.container.update();

        options.each(function(option) {
            el = new Element(type);
            el.writeAttribute('data-value', option);
            el.update(option);
            that.container.insert({ bottom: el });
        });


    },

    filter: function(value) {
        var that = this,
            currentValue = this.getCurrentValue();

        if (value == this.showAllLabel) {
            value == 'All';
        }

        this.data.getLocations().each(function(location) {
            that.testItem(location[that.attribute], value, location);
        });

        this.dispatch(Aligent.StoreFilter.FILTERED);
    },

    testItem: function(item, value, location) {
        if (item === value || (item instanceof Array && item.indexOf(value) !== -1) || (value == 'All' || value == this.showAllLabel) || location.alwaysShow == true) {
            if (typeof location.listItems != 'undefined') {
                location.listItems.each(function(item) {
                    item.show();
                });
            }
            if (typeof location.location != 'undefined') {
                location.location.show();
            }
        } else {
            if (typeof location.listItems != 'undefined') {
                location.listItems.each(function(item) {
                    item.hide();
                });
            }
            if (typeof location.location != 'undefined') {
                location.location.hide();
            }
        }
    },

    getOptions: function() {
        var options = [],
            that = this;

        if (this.showAllOption) {
            options.push(this.showAllLabel || 'All');
        }

        this.data.getLocations().each(function(item) {
            if (item[that.attribute] instanceof Array) {
                item[that.attribute].each(function(subItem) {
                    var tSubItem = subItem.trim();
                    if (tSubItem !== '' && options.indexOf(tSubItem) == -1) {
                        options.push(tSubItem);
                    }
                });
            } else if (typeof item[that.attribute] !== 'undefined') {
                var tItem = item[that.attribute];
                if (tItem !== '' && options.indexOf(tItem) == -1) {
                    options.push(tItem);
                }
            }
        });
        return options;
    }
});

Aligent.StoreFilter.FILTERED = 'filtered';

/* /////////////////////////////
 //// Search Form
 /////////////////////////////// */

Aligent.StoreSearch = Class.create(Aligent.EventManager, {

    initialize: function(form, config) {
        this.form = $$(form).first();

        this.geoValueElement = null;
        this.geoElementClickSelector = null;
        if (!config){
            config = {};
        }
        if (typeof(config.geoLocation) != 'object'){
            config.geolocation = {};
        }
        var geoLocation = config.geoLocation;
        if (geoLocation){
            if (geoLocation.geoValueElement){
                this.geoValueElement = geoLocation.geoValueElement;
            }
            if (geoLocation.geoElementClickSelector){
                this.geoElementClickSelector = geoLocation.geoElementClickSelector;
            }
            if (geoLocation.detectOnLoad){
                this.detectOnLoad = geoLocation.detectOnLoad;
            }

        }

        this.bindEvents();
        this.geoAjax = null;
        this.storeAjax = null;

        this._events = {};
        this.page = 0;
        this._config = typeof config !== 'undefined' ? config : {};
    },

    bindEvents: function() {
        this.form.observe('submit', this.formSubmit.bind(this));
        this.form.select('.check').invoke('observe', 'change', this.formSubmit.bind(this));
        if (this.geoValueElement && this.geoElementClickSelector){
            $(document).on("click", this.geoElementClickSelector, function(event, element) {
                this.recordDeviceLocation();
            }.bind(this));
        }
        if (this.detectOnLoad){
            this.recordDeviceLocation();
        }
    },
    recordDeviceLocation: function (){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(this.geoLocationProvided.bind(this));
        }
    },
    getRecordedLocation: function(){
        var location = {};
        if (this.geoValueElement){
            var element = $(this.geoValueElement);
            var jsonValue;
            if (element.value && (jsonValue = JSON.parse(element.value))){
                if (jsonValue.latitude && jsonValue.longitude){
                    location = jsonValue;
                }
            }
        }
        return location;
    },
    isSearchTermProvided: function (searchQuery){
        //Country Name is always part of search term, Need to check if some thing additional is provided
        var spacePosition = searchQuery.trim().indexOf(" ");
        if (spacePosition >1){
            return true;
        }
        return false;
    },
    formSubmit: function(ev) {
        if (ev){
            ev.stop();
        }
        var searchQuery = '';
        this.form.select('.combine').each(function(el) {
            searchQuery += el.value + ' ';
        });
        this.dispatch(Aligent.StoreSearch.GEOCODE_START);

        var params = { address: searchQuery };
        if (typeof this._config.region !== 'undefined') {
            params.region = this._config.region;
        }

        this.geoAjax = new Aligent.Ajax('/scripts/GeoCode.php', params, Aligent.Ajax.GET, 5);
        var location = this.getRecordedLocation();
        if (location && location.latitude && location.longitude && (!this.isSearchTermProvided(searchQuery))){
            var request =  this.buildAjaxRequest();
            request.latitude = location.latitude;
            request.longitude = location.longitude;
            this._search (request);
            Aligent.AnalyticsEvent.trackEvent('search', searchQuery);
            return;
        }

        this.geoAjax = new Aligent.Ajax('/scripts/GeoCode.php', { address: searchQuery }, Aligent.Ajax.GET, 5);
        this.geoAjax.listen(Aligent.Ajax.AJAX_SUCCESS, this.geoSuccess.bind(this));
        this.geoAjax.listen(Aligent.Ajax.AJAX_FAILED, this.geoFailed.bind(this));
        this.geoAjax.send();
        Aligent.AnalyticsEvent.trackEvent('search', searchQuery);
    },

    _buildRequest: function(lat, lng) {
        var request = {};
        request.latitude = lat;
        request.longitude = lng;

        this.form.select('.send').each(function(el) {
            request[el.name] = el.value;
        });

        this.form.select('.check').each(function(el) {
            if (el.checked) {
                if (!request[el.name]) {
                    request[el.name] = [];
                }
                request[el.name].push(el.value);
            }
        });

        if (typeof this._config.pageSize !== 'undefined') {
            request['page_size'] = this._config.pageSize;
        }

        request['page'] = this.page;

        return request;
    },

    _search: function(request) {
        this.dispatch(Aligent.StoreSearch.STORE_START);
        this.storeAjax = new Aligent.Ajax('/stores/index/search', request, Aligent.Ajax.POST, 3);
        this.storeAjax.listen(Aligent.Ajax.AJAX_SUCCESS, this.storeSuccess.bind(this));
        this.storeAjax.listen(Aligent.Ajax.AJAX_FAILED, this.storeFailed.bind(this));
        this.storeAjax.send();

    },
    geoLocationProvided: function (position) {
        if (position && position.coords && position.coords.longitude && position.coords.latitude) {
            var element = $(this.geoValueElement);
            element.value = JSON.stringify({latitude: position.coords.latitude, longitude: position.coords.longitude});
            this.formSubmit();
        }
    },
    searchCoordinates: function(lat, lng, page) {
        if (typeof page !== 'undefined') {
            this.page = page;
        }
        var request = this._buildRequest(lat, lng);
        this._search(request);
    },

    geoSuccess: function() {
        var location = this.geoAjax.getResponse();

        if (location === null) {
            this.geoFailed();
        } else {
            var request = this._buildRequest(location.lat, location.lng);
            this.dispatch(Aligent.StoreSearch.GEOCODE_SUCCESS);
            this._search(request);
        }
    },
    buildAjaxRequest: function() {
        var elements = this.form.elements;
        var element ;
        var AllValues = {};
        for (var prop in elements) {
            if (elements.hasOwnProperty(prop)){
                element = elements[prop];
                AllValues[element.name] = element.value;
            }
        }
        return AllValues;
    },

    geoFailed: function() {
        this.dispatch(Aligent.StoreSearch.GEOCODE_FAILURE);
        console.log('Geocode Failed');
        this.dispatch(Aligent.StoreSearch.COMPLETE);
    },

    storeSuccess: function() {
        this.dispatch(Aligent.StoreSearch.STORE_SUCCESS);
        Aligent.mapData.setData(this.storeAjax.getResponse());
        this.dispatch(Aligent.StoreSearch.COMPLETE);
    },

    storeFailed: function() {
        this.dispatch(Aligent.StoreSearch.STORE_FAILURE);
        this.dispatch(Aligent.StoreSearch.COMPLETE);
    }
});

Aligent.StoreSearch.STORE_START = 'storeStart';
Aligent.StoreSearch.STORE_SUCCESS = 'storeSuccess';
Aligent.StoreSearch.STORE_FAILURE = 'storeFailure';

Aligent.StoreSearch.GEOCODE_FAILURE = 'geocodeFailure';
Aligent.StoreSearch.GEOCODE_SUCCESS = 'geocodeSuccess';
Aligent.StoreSearch.GEOCODE_START = 'geocodeStart';
Aligent.StoreSearch.COMPLETE = 'storeSearchComplete';

/* /////////////////////////////
 //// Search Loading
 /////////////////////////////// */

Aligent.StoreSearchLoader = Class.create(Aligent.EventManager, {
    initialize: function(searchForm, container, loadingTemplate, errorTemplate, scrollToMap) {
        this._events = {};
        this.searchForm = searchForm;
        this.container = $$(container).first();
        this.loadingTemplate = Handlebars.compile($(loadingTemplate).innerHTML);
        this.errorTemplate = Handlebars.compile($(errorTemplate).innerHTML);
        this.scrollToMap = typeof scrollToMap !== 'undefined' ? scrollToMap : 'map';
        this.bindEvents();
    },

    bindEvents: function() {
        this.searchForm.listen(Aligent.StoreSearch.GEOCODE_START, this.onStart.bind(this));
        this.searchForm.listen(Aligent.StoreSearch.GEOCODE_FAILURE, this.onGeoError.bind(this));
        this.searchForm.listen(Aligent.StoreSearch.STORE_START, this.onStoreStart.bind(this));
        this.searchForm.listen(Aligent.StoreSearch.STORE_FAILURE, this.onStoreError.bind(this));
        this.searchForm.listen(Aligent.StoreSearch.STORE_SUCCESS, this.onSuccess.bind(this));
    },

    onStoreStart: function() {
        this.container.show();
        this.container.update(this.loadingTemplate({ message: 'Finding stores near your location' }));
    },

    onStart: function() {
        this.container.show();
        this.container.update(this.loadingTemplate({ message: 'Finding search location' }));
    },

    onGeoError: function() {
        this.container.show();
        this.container.update(this.errorTemplate({ message: 'There was an issue finding your search location, please try again later' }));
    },

    onStoreError: function() {
        this.container.show();
        this.container.update(this.errorTemplate({ message: 'There was an issue finding stores near your search location, please try again later' }));
    },

    onSuccess: function() {
        this.container.hide();
        if (this.scrollToMap) {
            Effect.ScrollTo(this.scrollToMap);
        }
    }

});

Aligent.AnalyticsEvent = {};
Aligent.AnalyticsEvent.categoryName = 'store-locator';

Aligent.AnalyticsEvent.trackEvent = function (action, label) {
    //push event to data layer of google tag manager is used.
    // Code in google tag manager should push information to google analytics
    if ((typeof(dataLayer) =='object') && (typeof(dataLayer.push) == 'function')){
        var eventName = Aligent.AnalyticsEvent.categoryName + '-' + action;
        dataLayer.push({'event': eventName,eventLabel:label});
    }
    //if google tag manager is not present try to push data to google analytics directly
    else if (typeof(ga) === 'function') {
        ga('send', 'event', Aligent.AnalyticsEvent.categoryName, action, label);
    }
};
//this is to avoid circular reference in JSON.stringify which throws error
Aligent.AnalyticsEvent.stringify = function (object){
    var simpleObject = {};
    for (var prop in object ){
        if (!object.hasOwnProperty(prop)){
            continue;
        }
        if (typeof(object[prop]) == 'object'){
            continue;
        }
        if (typeof(object[prop]) == 'function'){
            continue;
        }
        simpleObject[prop] = object[prop];
    }
    var cleanedJson = JSON.stringify(simpleObject) ;
    return cleanedJson;
};
